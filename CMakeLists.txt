cmake_minimum_required(VERSION 2.8.3)
project(eureca_popeye_assembly)

add_compile_options(-std=c++11 -DBOOST_MPL_CFG_NO_PREPROCESSED_HEADERS -DBOOST_MPL_LIMIT_VECTOR_SIZE=50 -DFUSION_MAX_VECTOR_SIZE=50)

set(CMAKE_BUILD_TYPE Debug)

find_package(catkin REQUIRED COMPONENTS
  actionlib_msgs control_msgs   geometry_msgs sensor_msgs   std_msgs  trajectory_msgs cartesian_msgs std_srvs

  moveit_visual_tools
  moveit_core
  moveit_ros_planning
  popeye_arm_moveit_config
  roscpp
  tf_helper
  eigen_matrix_utils
  cartesian_trajectory
  moveit_planning_helper
  diagnostic_aggregator
  diagnostic_common_diagnostics
  diagnostic_updater
  message_generation
  rosparam_utilities
)




################################################
## Declare ROS messages, services and actions ##
################################################
add_message_files( 	DIRECTORY 	msg
                    FILES		SimplePlan.msg
)
add_service_files( 	DIRECTORY 	srv
                    FILES		configure_planning_scene.srv
)
add_action_files(	DIRECTORY 	action
                    FILES     	Trigger.action
                                PickPlan.action
                                PickExecute.action

)
## Generate added messages and services with any dependencies listed here
generate_messages( DEPENDENCIES actionlib_msgs   control_msgs   geometry_msgs sensor_msgs std_msgs  trajectory_msgs cartesian_msgs std_srvs )




## System dependencies are found with CMake's conventions
catkin_package(
  INCLUDE_DIRS include
  CATKIN_DEPENDS
    geometry_msgs
    cartesian_msgs
    eureca_assembly_msgs
    std_srvs

    message_generation

    moveit_core
    moveit_ros_planning
    popeye_arm_moveit_config
    roscpp
    tf_helper

    eigen_matrix_utils
    cartesian_trajectory
    moveit_planning_helper
    diagnostic_aggregator
    diagnostic_common_diagnostics
    diagnostic_updater

    rosparam_utilities
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

######################################################################
add_library(${PROJECT_NAME}
  src/${PROJECT_NAME}/motion_manager.cpp
  src/${PROJECT_NAME}/task_manager.cpp
)
######################################################################

######################################################################
add_executable       (popeye_fsm_assembly_node src/popeye_fsm_assembly_node.cpp)
add_dependencies     (popeye_fsm_assembly_node ${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(popeye_fsm_assembly_node ${PROJECT_NAME} ${catkin_LIBRARIES} )
######################################################################

######################################################################
add_executable       (eureca_popeye_scene_configurator src/eureca_popeye_planning_test.cpp)
add_dependencies     (eureca_popeye_scene_configurator ${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(eureca_popeye_scene_configurator ${catkin_LIBRARIES} )
######################################################################

