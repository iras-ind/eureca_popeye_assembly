#include <limits>
#include <ros/ros.h>

#include <diagnostic_updater/diagnostic_updater.h>
#include <eureca_popeye_assembly/motion_manager.h>
#include <eureca_popeye_assembly/task_manager.h>

#include <diagnostic_updater/diagnostic_updater.h>
#include <std_msgs/Bool.h>
#include <diagnostic_updater/publisher.h>

#include <tf_conversions/tf_eigen.h>
#include <eureca_popeye_assembly/common.h>
#include <eureca_popeye_assembly/SimplePlan.h>
#include <eureca_popeye_assembly/PickPlanAction.h>

#include <moveit/planning_scene_interface/planning_scene_interface.h>


const std::string ROBOT_DESCRIPTION_NS = "robot_description";

double time_to_launch;

#define GET_AND_RETURN( nh, param_name, param )\
if (!nh.getParam(param_name,param))\
{\
  ROS_ERROR("Parameter '%s/%s' is not in the ros parame server. Go To Error State.", nh.getNamespace().c_str(), std::string( param_name ).c_str() );\
  return -1;\
}\

// ---------------------------------------------------------------------
#define LOOKUP_TRANSFORM( BASE_FRAME, TARGET_FRAME, TF_TRANSFORM)\
{\
  ros::Time st = ros::Time::now();\
  while( ros::ok() )\
  {\
    try\
    {\
      listener.lookupTransform( frames_id.at(BASE_FRAME), frames_id.at(TARGET_FRAME), ros::Time(0), TF_TRANSFORM);\
      break;\
    }\
    catch (tf::TransformException ex)\
    {\
      ROS_WARN_THROTTLE(5,"%s",ex.what());\
    }\
    if( (ros::Time::now() - st).toSec() > 20 )\
    {\
      ROS_ERROR_THROTTLE(5,"The tf listener didn't found any connections in 20 sec");\
      return -1;\
    }\
  }\
}\

// ---------------------------------------------------------------------

enum TrajectoriesID { APPROACH
                    , GRASP
                    , GRASP_FINALIZATION
                    , CARRY
                    , PLACE
                    , PLACE_FINALIZATION
                    , GO_TO_REST };



enum ObjectType {COLLISION_OBJECT, ROBOT_LINK };
typedef std::map<std::string, ObjectType > FramesInfo;
std::ostream &operator<<(std::ostream &os, FramesInfo const &f) { for( const auto p : f)  os << p.first << " " << ( p.second == COLLISION_OBJECT ? "COLLISION_OBJECT" :  "ROBOT_LINK" ); return os; }
std::ostream &operator<<(std::ostream &os, std::vector<std::string> const &f) { for( const auto p : f)  os << p << ", "; return os; }


enum FRAME_ID   { WORLD
                , GRIPPER
                , HATBOX
                , HATBOX_APPROACH
                , HATBOX_BRACKET
                , HATBOX_ESCAPE
                , FUSELAGE_BRACKET_1
                , FUSELAGE_APPROACH_1
                , FUSELAGE_FINAL_1    
                , FTSENSOR_FLANGE };

const std::map<FRAME_ID, std::string > frames_id  = { { WORLD               , "world"                      }
                                                    , { GRIPPER             , "hatbox_clamper"             }
                                                    , { HATBOX              , "hatbox"                     }
                                                    , { HATBOX_APPROACH     , "hatbox_approach"            }
                                                    , { HATBOX_BRACKET      , "hatbox_bracket"             }
                                                    , { HATBOX_ESCAPE       , "hatbox_escape"              }
                                                    , { FUSELAGE_BRACKET_1  , "fuselage_hatbox_bracket_1"  }
                                                    , { FUSELAGE_APPROACH_1 , "fuselage_hatbox_approach_1" }
                                                    , { FUSELAGE_FINAL_1    , "fuselage_hatbox_final_1"    }
                                                    , { FTSENSOR_FLANGE     , "ftsensor_flange"            } };


/**
 * @brief main
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv)
{

  ros::init(argc, argv, "eureca_popeye_assembly_node");
  ros::NodeHandle nh("~");
  ros::AsyncSpinner spinner(4);
  spinner.start();

  ROS_INFO("***** FSM ASSEMBLY NODE ********");

  std::string group_name              ;
  bool        use_ikfast              ;
  std::string robot_description       ;
  std::string base_frame_name         ;
  std::string tool_frame_name         ;


  GET_AND_RETURN( nh, "group_name"        , group_name             )
  GET_AND_RETURN( nh, "robot_description" , robot_description      )
  GET_AND_RETURN( nh, "use_ikfast"        , use_ikfast             )
  if( use_ikfast  )
  {
    GET_AND_RETURN( nh, group_name + "/ikfast_base_frame" , base_frame_name      )
    GET_AND_RETURN( nh, group_name + "/ikfast_tool_frame" , tool_frame_name      )
  }
  
  robot_model_loader::RobotModelLoader robot_model_loader( robot_description );
  robot_model::RobotModelPtr           robot_model = robot_model_loader.getModel();
  
  base_frame_name = robot_model->getJointModelGroup(group_name)->getSolverInstance()->getBaseFrame();
  tool_frame_name = robot_model->getJointModelGroup(group_name)->getSolverInstance()->getTipFrame();


  tf::TransformListener listener;



  eureca_popeye_assembly::PickPlanGoal goal;

  std::vector< TrajectoriesID > trajectories_to_plan_ids =  { APPROACH
                                                            , GRASP
                                                            , GRASP_FINALIZATION
                                                            , CARRY
                                                            , PLACE
                                                            , PLACE_FINALIZATION
                                                            , GO_TO_REST };
  FRAME_ID END_EFFECTOR;
  const auto & it = std::find_if(frames_id.begin(), frames_id.end(), [&]( const std::pair<FRAME_ID, std::string >& p) { return p.second == tool_frame_name; } );
  if( it ==  frames_id.end() )
  {
    ROS_FATAL("Error in the definition of the end effector frame. Requested: %s", tool_frame_name.c_str() );
    return -1;
  }
  END_EFFECTOR = it->first;
  tf::StampedTransform T_e_g_ ;   LOOKUP_TRANSFORM( END_EFFECTOR    , GRIPPER             , T_e_g_  );
  tf::StampedTransform T_0_h_ ;   LOOKUP_TRANSFORM( WORLD           , HATBOX              , T_0_h_  );
  tf::StampedTransform T_0_ha_;   LOOKUP_TRANSFORM( WORLD           , HATBOX_APPROACH     , T_0_ha_ );
  tf::StampedTransform T_0_he_;   LOOKUP_TRANSFORM( WORLD           , HATBOX_ESCAPE       , T_0_he_ );
  tf::StampedTransform T_h_hb_;   LOOKUP_TRANSFORM( HATBOX          , HATBOX_BRACKET      , T_h_hb_ );
  tf::StampedTransform T_0_1A_;   LOOKUP_TRANSFORM( WORLD           , FUSELAGE_APPROACH_1 , T_0_1A_ );
  tf::StampedTransform T_0_1B_;   LOOKUP_TRANSFORM( WORLD           , FUSELAGE_BRACKET_1  , T_0_1B_ );
  tf::StampedTransform T_0_1F_;   LOOKUP_TRANSFORM( WORLD           , FUSELAGE_FINAL_1    , T_0_1F_ );
  tf::StampedTransform T_0_e0_;   LOOKUP_TRANSFORM( WORLD           , FTSENSOR_FLANGE     , T_0_e0_ );

  tf::Transform T_g_h_  ; T_g_h_  .setIdentity(); // {h}atbox in {g}ripper 
  tf::Transform T_g_ha_ ; T_g_ha_ .setIdentity(); // {h}atbox {a}pproach in {g}ripper 
  tf::Transform T_g_he_ ; T_g_he_ .setIdentity(); // {h}atbox {e}scape   in {g}ripper
  tf::Transform T_hb_1A_; T_hb_1A_.setIdentity(); // {h}atbox {b}racket  in {1}st {A}pproach assembly position of the fuselage
  tf::Transform T_hb_1B_; T_hb_1B_.setIdentity(); // {h}atbox {b}racket  in {1}st {B}racket position of the fuselage (hook insertion, no rotation)
  tf::Transform T_hb_1F_; T_hb_1F_.setIdentity(); // {h}atbox {b}racket  in {1}st {F}inal Bracket position of the fuselage (hook insertion, and rotation)
    
  for( const auto & trajctory_id : trajectories_to_plan_ids )
  {
    tf::Transform T_0_e, T_0_g;
    
    switch(trajctory_id)
    {
      case APPROACH:
      {
        eureca_popeye_assembly::SimplePlan plan_data;
       
        T_0_g = T_0_ha_ * T_g_ha_.inverse();                     
        T_0_e = T_0_g   * T_e_g_.inverse();
        
        tf::transformTFToMsg( T_0_e, plan_data.target_frame );

        plan_data.object_id                = "object_id";
        plan_data.allowed_collision_link1  = { "hatbox_cart" };
        plan_data.allowed_collision_link2  = { "hatbox" };
        plan_data.controller_configuration = "thor";
        plan_data.planner_id               = "default";

        goal.approach = plan_data;
      }
      break;
      case GRASP:
      { // Grasping
        eureca_popeye_assembly::SimplePlan plan_data;
        
        T_0_g  = T_0_h_ * T_g_h_.inverse();
        T_0_e  = T_0_g  * T_e_g_.inverse();
        
        tf::transformTFToMsg( T_0_e, plan_data.target_frame );
        plan_data.object_id                = "hatbox";
        plan_data.allowed_collision_link1  = { "hatbox_cart", "hatbox"         };
        plan_data.allowed_collision_link2  = { "hatbox"     , "hatbox"         };
        plan_data.controller_configuration = "follow_joint_trajectory_impedance";
        plan_data.planner_id               = "descartes";

        goal.grasp = plan_data;
      }
      break;
      case GRASP_FINALIZATION:
      { // grasp_finalization
        eureca_popeye_assembly::SimplePlan plan_data;
        
        T_0_g = T_0_he_ * T_g_he_.inverse();                     
        T_0_e = T_0_g   * T_e_g_ .inverse();
        tf::transformTFToMsg( T_0_e, plan_data.target_frame );
        
        plan_data.object_id                = "object_id";
        plan_data.allowed_collision_link1  = { "hatbox"         };
        plan_data.allowed_collision_link2  = { "hatbox_clamper" };
        plan_data.controller_configuration = "follow_joint_trajectory_impedance";
        plan_data.planner_id               = "descartes";

        goal.grasp_finalization = plan_data;
      }
      break;
      case CARRY:
      { // carry
        eureca_popeye_assembly::SimplePlan plan_data;
        
        T_0_g  = T_0_1A_ * T_hb_1A_.inverse( ) * T_h_hb_.inverse() * T_g_h_.inverse();
        T_0_e  = T_0_g   * T_e_g_.inverse();

        tf::transformTFToMsg( T_0_e, plan_data.target_frame );
        
        plan_data.object_id                = "object_id";
        plan_data.controller_configuration = "thor";
        plan_data.planner_id               = "default";

        goal.carry = plan_data;
      }
      break;
      case PLACE:
      { // place
        eureca_popeye_assembly::SimplePlan plan_data;
        tf::Transform T_0_hb;

        
        T_0_g  = T_0_1B_ * T_hb_1B_.inverse() * T_h_hb_.inverse() * T_g_h_.inverse();
        T_0_e  = T_0_g * T_e_g_.inverse();

        tf::transformTFToMsg( T_0_e, plan_data.target_frame );
        
        plan_data.object_id                = "object_id";
        plan_data.controller_configuration = "follow_joint_trajectory_impedance";
        plan_data.planner_id               = "descartes";

        goal.place = plan_data;
      }
      break;
      case PLACE_FINALIZATION:
      { // place finalization
        eureca_popeye_assembly::SimplePlan plan_data;
        tf::Transform        T_0_hb;

        T_0_g = T_0_1F_ * T_hb_1F_.inverse() * T_h_hb_.inverse() * T_g_h_.inverse();
        T_0_e = T_0_g * T_e_g_.inverse();

        tf::transformTFToMsg( T_0_e, plan_data.target_frame );
        plan_data.object_id                = "object_id";
        plan_data.controller_configuration = "follow_joint_trajectory_impedance";
        plan_data.planner_id               = "descartes";

        goal.place_finalization = plan_data;
      }
      break;
      case GO_TO_REST:
      { // go to rest
        eureca_popeye_assembly::SimplePlan plan_data;
        tf::Transform T_0_e;

        T_0_e = T_0_e0_;
        T_0_g = T_0_e * T_e_g_;

        tf::transformTFToMsg( T_0_e, plan_data.target_frame );

        plan_data.object_id                = "object_id";
        plan_data.controller_configuration = "follow_joint_trajectory_impedance";
        plan_data.planner_id               = "descartes";

        goal.go_to_rest = plan_data;
      }
      break;
    }
    ROS_INFO_STREAM( BOLDYELLOW() << "T_0_g:\n" << RESET() << T_0_g );
    ROS_INFO_STREAM( BOLDYELLOW() << "T_0_e:\n" << RESET() << T_0_e );

  }

  std::shared_ptr<eureca_popeye_assembly::motion::StateMachine> motion_fsm = std::make_shared<eureca_popeye_assembly::motion::StateMachine>( nh );
  motion_fsm->start();

  std::shared_ptr<eureca_popeye_assembly::task::StateMachine>   task_fsm  = std::make_shared<eureca_popeye_assembly::task::StateMachine>( nh );
  task_fsm->setMotionManager(motion_fsm);
  task_fsm->start();
  
  ROS_INFO("ask to pick");
  task_fsm->process_event(eureca_popeye_assembly::task::pickPlanRequest(goal));
  if (getStateName(*task_fsm).compare("Wait"))
  {
    ROS_ERROR("Plan failed");
    return 0;
  }
  
  eureca_popeye_assembly::PickExecuteGoal picking_exec_goal;
  picking_exec_goal.object_frame="object_id";
  
  task_fsm->process_event( eureca_popeye_assembly::task::pickExecuteRequest(picking_exec_goal) );
  ROS_INFO("plan");
  for (int idx=0;idx<100;idx++)
  {
    task_fsm->process_event(eureca_popeye_assembly::task::update());
    ros::Duration(0.1).sleep();
  }

  return 0;
}



