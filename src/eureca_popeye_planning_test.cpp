#include <thread>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
// MoveIt!
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/kinematic_constraints/utils.h>
#include <rosparam_utilities/rosparam_utilities.h>
#include <eureca_popeye_assembly/configure_planning_scene.h>
#include <moveit_planning_helper/manage_planning_scene.h>
#include <eigen3/Eigen/Core>
#include <tf_conversions/tf_eigen.h>


const std::string PATH_TO_MESH_NS                        = "package://eureca_description/meshes/components";
const std::string ROBOT_DESCRIPTION_NS                   = "robot_description";
const std::string HATBOX_OBJECT_ID                       = "hatbox";
const std::string HATBOX_CART_OBJECT_ID                  = "hatbox_cart";
const std::string HATBOX_IN_HABOX_CART_FRAME_NS          = "hatbox_in_habox_cart_frame";
const std::string HATBOX_APPROACH_IN_HABOX_FRAME_NS      = "hatbox_approach_in_hatbox_frame";
const std::string HATBOX_BRACKET_IN_HABOX_FRAME_NS       = "hatbox_bracket_in_hatbox_frame";
const std::string HATBOX_POST_GRASPING_Z_DISPLACEMENT_NS = "hatbox_post_grasping_z_displacement";
const std::string WINDOW_TO_ATTACH_POSE_NS               = "window_to_attach_pose";
const std::string CONFIGURE_SCENE_SRV                    = "configure_scene";
const std::string PLANNING_SCENE_TOPIC_NS                = "planning_scene";
const std::string GROUP_NAME_NS                          = "group_name";


bool getParam( ros::NodeHandle& nh, const std::string key, tf::Pose& pose)
{
  XmlRpc::XmlRpcValue config;
  if(!nh.getParam(key,config))
  {
    ROS_FATAL_STREAM("Parameter '" << nh.getNamespace() << "/" << key << "' not found!");
    return false;
  }

  std::vector<double> xyz;
  if( !rosparam_utilities::getParamVector( nh, key + "/xyz", xyz ) )
  {
    ROS_FATAL_STREAM("Parameter '" << nh.getNamespace() << "/" << key << "'/xyz' is malformed. " );
    return false;
  }
  std::vector<double> rpy;
  if( !rosparam_utilities::getParamVector( nh, key + "/rpy", rpy ) )
  {
    ROS_FATAL_STREAM("Parameter '" << nh.getNamespace() << "/" << key << "'/rpy' is malformed. " );
    return false;
  }

  Eigen::Affine3d value;
  value = Eigen::Affine3d::Identity();
  value = Eigen::AngleAxisd(rpy[2], Eigen::Vector3d::UnitZ() )
        * Eigen::AngleAxisd(rpy[1], Eigen::Vector3d::UnitY() )
        * Eigen::AngleAxisd(rpy[0], Eigen::Vector3d::UnitX() );
  value.translation() = Eigen::Vector3d(xyz[0],xyz[1],xyz[2]);

  tf::transformEigenToTF( value, pose );
  return true;
}

class PlanningSceneConfigurator
{
  ros::NodeHandle nh_;
  std::shared_ptr< robot_model_loader::RobotModelLoader >  robot_model_loader_;
  robot_model::RobotModelPtr                               robot_model_;
  std::string                                              group_name_;
  std::string                                              robot_description_;
  std::string                                              robot_tip_;
  moveit::planning_interface::PlanningSceneInterface       planning_scene_interface_;
  double                                                   z_displacement_;

  ros::ServiceServer                                        configuration_srv_;

  tf::Pose    T_0_h_;   // from world  to hatbox
  tf::Pose    T_0_hb_;  // from world  to hatbox_bracket
  tf::Pose    T_0_ha_;  // from world  to hatbox_approach
  tf::Pose    T_0_hc_;  // from world  to hatbox_cart
  tf::Pose    T_hc_h_;  // from hatbox to hatbox_cart
  tf::Pose    T_h_ha_;  // from hatbox to hatbox_approach
  tf::Pose    T_h_hb_;  // from hatbox to hatbox_bracket
  tf::Pose    T_0_he_;  // habox escape frame

  bool                             join_thread_;
  tf::TransformBroadcaster         br_;
  std::shared_ptr< std::thread >   br_thread_;

  void threadTFBroadcaster( )
  {
    ros::Rate rt(100);

    bool first_cycle = true;
    do 
    {
      ros::Time t = ros::Time::now();
      auto hatbox_pose = planning_scene_interface_.getObjectPoses( {HATBOX_OBJECT_ID, HATBOX_CART_OBJECT_ID});
      tf::poseMsgToTF( hatbox_pose[HATBOX_OBJECT_ID     ] , T_0_h_);
      tf::poseMsgToTF( hatbox_pose[HATBOX_CART_OBJECT_ID] , T_0_hc_);
      
      T_0_hb_ = T_0_h_ * T_h_hb_;
      T_0_ha_ = T_0_h_ * T_h_ha_;
      
      if( first_cycle )
      { 
        T_0_he_  = T_0_h_; 
        T_0_he_.getOrigin() += tf::Vector3(0,0,z_displacement_);
      }
      
      br_.sendTransform(tf::StampedTransform(T_0_h_ , t, "world", "hatbox"));
      br_.sendTransform(tf::StampedTransform(T_0_hb_, t, "world", "hatbox_bracket"));
      br_.sendTransform(tf::StampedTransform(T_0_ha_, t, "world", "hatbox_approach"));
      br_.sendTransform(tf::StampedTransform(T_0_hc_, t, "world", "hatbox_cart"));
      br_.sendTransform(tf::StampedTransform(T_0_he_, t, "world", "hatbox_escape"));
      
      first_cycle = false;
      rt.sleep();
    } while(ros::ok() && !join_thread_  );

  }

  bool configureScene( eureca_popeye_assembly::configure_planning_scene::Request&   req
                     , eureca_popeye_assembly::configure_planning_scene::Response&  res )
  {
    std::vector< moveit_msgs::CollisionObject > objs;

    ROS_INFO_STREAM( " Add Collision Objects" );
    tf::Pose T_0_h;
    tf::poseMsgToTF( req.hatbox_pose, T_0_h );
    ROS_INFO_STREAM( " T_0_h:" << req.hatbox_pose );
    objs.push_back( *moveit_planning_helper::toCollisionObject ( HATBOX_OBJECT_ID
                                                                , PATH_TO_MESH_NS + "/collision/hatbox.stl"
                                                                , "world"
                                                                , T_0_h
                                                                , Eigen::Vector3d(1,1,1) ) );

    tf::Pose T_0_hc = T_0_h * T_hc_h_.inverse();

    objs.push_back( *moveit_planning_helper::toCollisionObject ( HATBOX_CART_OBJECT_ID
                                                                , PATH_TO_MESH_NS + "/collision/hatbox_cart.stl"
                                                                , "world"
                                                                , T_0_hc
                                                                , Eigen::Vector3d(1,1,1) ) );

    ROS_INFO_STREAM( " Set colors" );
    moveit_msgs::ObjectColor color_hatbox     ; color_hatbox      .id=HATBOX_OBJECT_ID     ; color_hatbox      .color.r = 255; color_hatbox      .color.g = 255; color_hatbox      .color.b = 255; color_hatbox     .color.a = 255;
    moveit_msgs::ObjectColor color_hatbox_cart; color_hatbox_cart .id=HATBOX_CART_OBJECT_ID; color_hatbox_cart .color.r = 120; color_hatbox_cart .color.g = 120; color_hatbox_cart .color.b = 120; color_hatbox_cart.color.a = 255;

    ROS_INFO_STREAM( " applyAndCheckPS" );
    if( !moveit_planning_helper::applyAndCheckPS( ros::NodeHandle()
                                                , objs
                                                , {color_hatbox, color_hatbox_cart}
                                                , ros::Duration(10) ) )
    {
      ROS_FATAL_STREAM("Failed in uploading the collision objects");
      res.success = false;
    }
    else
    {
      ROS_INFO_STREAM("Ok! Objects loaded in the planning scene");
      res.success = true;
    }

    if( br_thread_ == nullptr )
    {
      join_thread_ = false;
      br_thread_.reset( new std::thread( &PlanningSceneConfigurator::threadTFBroadcaster, this ) );
    }

    return true;
  }

public:
  PlanningSceneConfigurator(  )
    : nh_                ( "~" )
  {
      if( !getParam( nh_, HATBOX_APPROACH_IN_HABOX_FRAME_NS, T_h_ha_ ) )
      {
        throw std::runtime_error("Error in extracting param. Abort" );
      }

      if( !getParam( nh_, HATBOX_IN_HABOX_CART_FRAME_NS, T_hc_h_ ) )
      {
        throw std::runtime_error("Error in extracting param. Abort" );
      }


      if( !getParam( nh_, HATBOX_BRACKET_IN_HABOX_FRAME_NS, T_h_hb_ ) )
      {
        throw std::runtime_error("Error in extracting param. Abort" );
      }
      
      if( !nh_.getParam( HATBOX_POST_GRASPING_Z_DISPLACEMENT_NS, z_displacement_ ) )
      {
        std::stringstream str; str << "Error in extracting param '" << nh_.getNamespace() <<"/"<<HATBOX_POST_GRASPING_Z_DISPLACEMENT_NS<< "'. Abort.";
        throw std::runtime_error( str.str().c_str() );
      }
      
      
      if( !nh_.getParam( GROUP_NAME_NS, group_name_ ) )
      {
        std::stringstream str; str << "Error in extracting param '" << nh_.getNamespace() <<"/"<<GROUP_NAME_NS<< "'. Abort.";
        throw std::runtime_error( str.str().c_str() );
      }
      
      if( !nh_.getParam( ROBOT_DESCRIPTION_NS, robot_description_ ) )
      {
        std::stringstream str; str << "Error in extracting param '" << nh_.getNamespace() <<"/"<<ROBOT_DESCRIPTION_NS<< "'. Abort.";
        throw std::runtime_error( str.str().c_str() );
      }
      
      robot_model_loader_.reset( new robot_model_loader::RobotModelLoader ( robot_description_  ) );
      robot_model_                = robot_model_loader_->getModel();
      configuration_srv_          = nh_.advertiseService(CONFIGURE_SCENE_SRV, &PlanningSceneConfigurator::configureScene, this);
      
      robot_tip_ = robot_model_->getJointModelGroup(group_name_)->getSolverInstance()->getTipFrame();

      ROS_INFO_STREAM("Robot Tip: " << robot_tip_ );
      
//      if( !getParam( nh_, WINDOW_TO_ATTACH_POSE_NS, T_hc_h_ ) )
//      {
//        throw std::runtime_error("Error in extracting param. Abort" );
//      }
      br_thread_  = nullptr;

  }

  ~PlanningSceneConfigurator()
  {
    join_thread_ = true;
    br_thread_->join();
  }

};

int main(int argc, char** argv)
{

  ros::init(argc,argv,"eureca_popeye_assembly_test");
  ros::AsyncSpinner spinner(1);
  spinner.start();

  PlanningSceneConfigurator planning_scene_configurator;

  ros::waitForShutdown();
  return 0;
}
