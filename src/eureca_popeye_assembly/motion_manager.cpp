#include <eureca_popeye_assembly/motion_manager.h>




namespace eureca_popeye_assembly
{
namespace motion
{

void StateMachineStructure::entryStateError            ( void ) { }
void StateMachineStructure::entryStateRest             ( void ) { }
void StateMachineStructure::entryStateReady            ( void ) { }
void StateMachineStructure::entryStateApproach         ( void ) { }
void StateMachineStructure::entryStateGrasp            ( void ) { }
void StateMachineStructure::entryStateCloseGripper     ( void ) { }
void StateMachineStructure::entryStateGraspFinalization( void ) { }
void StateMachineStructure::entryStateCarry            ( void ) { }
void StateMachineStructure::entryStatePlace            ( void ) { }
void StateMachineStructure::entryStatePlaceFinalization( void ) { }
void StateMachineStructure::entryStateOpenGripper      ( void ) { }

void StateMachineStructure::exitStateError            ( void ) { }
void StateMachineStructure::exitStateInit             ( void ) { }
void StateMachineStructure::exitStateRest             ( void ) { }
void StateMachineStructure::exitStateReady            ( void ) { }
void StateMachineStructure::exitStateApproach         ( void ) { }
void StateMachineStructure::exitStateGrasp            ( void ) { }
void StateMachineStructure::exitStateCloseGripper     ( void ) { }
void StateMachineStructure::exitStateGraspFinalization( void ) { }
void StateMachineStructure::exitStateCarry            ( void ) { }
void StateMachineStructure::exitStatePlace            ( void ) { }
void StateMachineStructure::exitStatePlaceFinalization( void ) { }
void StateMachineStructure::exitStateOpenGripper      ( void ) { }

void StateMachineStructure::entryStateInit()
{
  go_to_error_=false;

  std::string group_name;
  if (!nh_.getParam("group_name",group_name))
  {
    ROS_ERROR("%s/group_name' not exist", nh_.getNamespace().c_str());
    go_to_error_=true;
    return;
  }
  
  bool use_ikfast;
  if (!nh_.getParam("use_ikfast",use_ikfast))
  {
    ROS_ERROR("%s/use_ikfast not exist", nh_.getNamespace().c_str());
    go_to_error_=true;
    return;
  }

  std::string tf_ns = use_ikfast ? group_name + "/ikfast_tool_frame" : group_name + "/tool_frame";
  std::string bf_ns = use_ikfast ? group_name + "/ikfast_base_frame" : group_name + "/base_frame";
  if (!nh_.getParam(tf_ns,tool_frame_name_))
  {
    ROS_ERROR("%s/%s not exist", nh_.getNamespace().c_str(), tf_ns.c_str() );
    go_to_error_=true;
    return;
  }

  trj_action_.reset(new actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction>(nh_, "/" + group_name+"/follow_joint_trajectory"));
  if (!trj_action_->waitForServer(ros::Duration(10)))
  {
    ROS_ERROR("NO %s server", (group_name+"/follow_joint_trajectory").c_str() );
    go_to_error_ = true;
    return;
  } 
}

void StateMachineStructure::actionCachePlan (const pickRequest &event )
{
  trajectories_ = event.goals_;
}

void StateMachineStructure::actionStartApproach(none const & e )
{
// load the correct control configuration correct TODO
  trj_action_->sendGoal( trajectories_[motion::JointTrajectory::APPROACH]->goal_);
  start_goal_ = ros::Time::now();
}


void StateMachineStructure::actionStartGrasp(update const & e )
{
// load the correct control configuration correct TODO
  trj_action_->sendGoal( trajectories_[motion::JointTrajectory::GRASP]->goal_);
  start_goal_ = ros::Time::now();
}


void StateMachineStructure::actionTriggerCloseGripper(update const & e )
{
// attivare un client che chiede ad un server l'abilitazione da tastiera per andare avanti per popeye
}

void StateMachineStructure::actionStartGraspFinalization(update      const & e )
{
// load the correct control configuration correct TODO
  trj_action_->sendGoal( trajectories_[motion::JointTrajectory::GRASP_FINALIZATION]->goal_ );
  start_goal_ = ros::Time::now();
}

void StateMachineStructure::actionStartCarry(none        const & e )
{
// load the correct control configuration correct TODO
  trj_action_->sendGoal( trajectories_[motion::JointTrajectory::CARRY]->goal_ );
  start_goal_ = ros::Time::now();
}

void StateMachineStructure::actionStartPlace(update      const & e )
{
// load the correct control configuration correct TODO
  trj_action_->sendGoal( trajectories_[motion::JointTrajectory::PLACE]->goal_ );
  start_goal_ = ros::Time::now();
}

void StateMachineStructure::actionFinalizePlace ( update      const & e  )
{
// load the correct control configuration correct TODO
  trj_action_->sendGoal( trajectories_[motion::JointTrajectory::PLACE_FINALIZATION]->goal_ );
  start_goal_ = ros::Time::now();
}
void StateMachineStructure::actionTriggerOpenGripper ( none        const & e )
{
// attivare un clien che chiede ad un server l'abilitazione da tastiera per andare avanti per popeye
}
void StateMachineStructure::actionGoToRest( none        const & e  )
{
// load the correct control configuration correct TODO
  trj_action_->sendGoal( trajectories_[motion::JointTrajectory::GO_TO_REST]->goal_ );
  start_goal_ = ros::Time::now();
}

bool StateMachineStructure::guardIsMovementFinished  (  update     const & e  )
{
  trj_action_->getState().isDone();
}
bool StateMachineStructure::guardIsGrasped           ( update     const & e  )
{
  (ros::Time::now() - start_goal_);
}
bool StateMachineStructure::guardIsPlaced            ( none       const & e )
{
  (ros::Time::now() - start_goal_);
}
bool StateMachineStructure::guardIsGraspingFinished  ( update     const & e )
{
  (ros::Time::now() - start_goal_);
}
bool StateMachineStructure::guardIsPickingFinished   ( update     const & e )
{
  (ros::Time::now() - start_goal_);
}
bool StateMachineStructure::guardIsInError           ( update     const & e )
{
  go_to_error_;
}



}
}
