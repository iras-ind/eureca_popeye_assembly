#include <random>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <functional>
#include <fstream>
#include <ctime>
#include "ros/ros.h"

#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/conversions.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit_msgs/GetPlanningScene.h>
#include <moveit_msgs/ApplyPlanningScene.h>
#include <moveit_planning_helper/manage_trajectories.h>

#include <control_msgs/FollowJointTrajectoryAction.h>
#include <shape_msgs/Mesh.h>

#include <descartes_moveit/ikfast_moveit_state_adapter.h>
#include <descartes_trajectory/axial_symmetric_pt.h>
#include <descartes_trajectory/cart_trajectory_pt.h>
#include <descartes_trajectory/joint_trajectory_pt.h>
#include <descartes_planner/dense_planner.h>
#include <descartes_utilities/ros_conversions.h>
#include <cartesian_path_utils/cartesian_path_utils.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include "geometric_shapes/shape_operations.h"
#include <eigen_conversions/eigen_msg.h>
#include <actionlib/client/simple_action_client.h>
#include <eureca_assembly_msgs/executeTrajectoryAction.h>

#include <eureca_assembly_msgs/planAssembly.h>
#include <eureca_assembly_msgs/planConnection.h>
#include <eureca_assembly_msgs/planCorrection.h>
#include <eureca_assembly/common.h>

template< typename T > std::string to_string  ( const std::vector< T >& v
                                              , const std::string prefix = "["
                                              , const std::string delimeter = ", "
                                              , const std::string terminator ="]" )
{
  std::string ret = prefix;
  if(v.size() == 0)
    return "";
  
  for( size_t i=0; i < v.size()-1; i++)
    ret += std::to_string( v[i] ) + delimeter;
  ret += std::to_string( v.back() ) + terminator;
  
  return ret;
}



static const std::string CART_ID 	= "panel_cart";
static const std::string PANEL_ID 	= "side_panel";
static const std::string FUSELAGE_ID 	= "fuselage";

static const std::string GAZEBO_PATH          = ros::package::getPath("eureca_gazebo");
static const std::string CART_PATH            = "package://eureca_gazebo/models/PanelCart/meshes/visual/panel_cart.stl";
static const std::string PANEL_PATH           = "package://eureca_gazebo/models/CargoPanelUpper/meshes/cargo_panel_upper.stl";//cargo
static const std::string STIIMA_PANEL_PATH    = "package://eureca_gazebo/models/SidePanel/meshes/side_panel_stiima.stl";
static const std::string FUSELAGE_PATH        = "package://eureca_gazebo/models/Fuselage/meshes/demonstrator_lower_collision.stl"; // cargo
static const std::string STIIMA_FUSELAGE_PATH = "package://eureca_gazebo/models/Fuselage/meshes/fuselage_stiima.stl";

static const std::string PLANNING_GROUP       = "manipulator";
static const std::string GRIP_ENDEFFECTOR     = "EE_link_grip";
static const std::string BASE_LINK            = "olivia_link_0";


class planner
{
protected:
    ros::NodeHandle nh_;
    moveit::planning_interface::MoveGroupInterface::Plan my_plan_;
    robot_model::RobotModelPtr robot_model_;
    kinematics::KinematicsBasePtr solver_;
    moveit_msgs::MoveItErrorCodes error_code_;
    boost::shared_ptr<descartes_moveit::IkFastMoveitStateAdapter> iiwa_model_;
    descartes_planner::DensePlanner iiwa_planner_;
    
    moveit_msgs::CollisionObject collision_object_panel_;
    moveit_msgs::CollisionObject collision_object_cart_;
    moveit_msgs::CollisionObject collision_object_cabina_;
    std::vector<std::string> object_ids_;
    std::vector<moveit_msgs::CollisionObject> cov_;
    bool scene_configurated_;
    bool calito_fuselage_;

    sensor_msgs::JointState js_;
    ros::Subscriber sub_;
    void currentJoints(const sensor_msgs::JointState::ConstPtr js)
    {
      js_.name.clear();
      js_.velocity.clear();
      js_.effort.clear();
      js_.position.clear();
      js_ = *js;
    } 

    ros::Publisher move_robot_to_start_state_;
    sensor_msgs::JointState start_state_msg_;
    
public:

    geometry_msgs::Pose currentPose_;
    bool newData_;
    tf::StampedTransform worldToRobot_;

    moveit::planning_interface::MoveGroupInterface      move_group_;
    moveit::planning_interface::PlanningSceneInterface  planning_scene_interface_;
    std::string                                         planning_frame_;
    std::vector<double>                                 joint_home_;
    std::vector<double>                                 panel_offset_vec_;
    std::vector<double>                                 panel_grip_offset_vec_;
    std::vector<double>                                 cabina_attach_offset_vec_;
    std::vector<double>                                 cabina_offset_vec_;
    std::vector<double>                                 stiima_cabina_offset_vec_;
    double                                              alpha_;
    double                                              default_velocity_;
    double                                              panel_velocity_;
    tf::Pose                                            cart_TO_sidePanel_;
    tf::Pose                                            attachPoint_TO_fuselageOrigin_;
    bool                                                execute_plan_motion_;
    
    planner ( ros::NodeHandle nh )
        : nh_ ( nh )
        , move_group_( PLANNING_GROUP )
        , scene_configurated_( false )
    {
      
      planning_frame_ = move_group_.getPlanningFrame();
      move_robot_to_start_state_ = nh_.advertise<sensor_msgs::JointState>("move_group/fake_controller_joint_states", 1000);
      std::cout << "PLANNING FRAME : " << planning_frame_ << std::endl;

      iiwa_model_.reset( ( new descartes_moveit::IkFastMoveitStateAdapter ));
      
      sub_ = nh_.subscribe("/iiwa/olivia_joint_states", 1, &planner::currentJoints, this);
    }


    void noSuccess ( moveit::planning_interface::MoveGroupInterface *move_group,
                     moveit_msgs::CollisionObject *collision_object,
                     moveit::planning_interface::PlanningSceneInterface *planning_scene_interface,
                     std::vector<std::string> *object_ids)
    {
        ROS_ERROR ( "pianificazione non riuscita" );
        move_group->detachObject ( collision_object->id );
        ros::Duration ( 1.0 ).sleep();
        planning_scene_interface->removeCollisionObjects ( *object_ids );
        if(!setRobotState(joint_home_))
          ROS_ERROR("Robot not in home position !");
        
    }


    bool configurationCallback  ( eureca_assembly_msgs::planAssemblyRequest& req
                                , eureca_assembly_msgs::planAssemblyResponse& res )
    {
            //------------------------------carico le posizioni da .config-------------------------------------------------------------------------
      if ( !nh_.getParam ( "default_positions/jointHome", joint_home_ ) )
      {
          ROS_ERROR ( "posizione jointHome non caricata" );
          return false;
      }
      
      if ( !nh_.getParam ( "default_positions/panelOffset", panel_offset_vec_ ) )
      {
          ROS_ERROR ( "posizione panelOffset non caricata" );
          return false;
      }
      
      ros::Duration(5).sleep();
      if ( !nh_.getParam ( "offset_positions/panelGripOffset", panel_grip_offset_vec_ ) )
      {
          ROS_ERROR ( "posizione panelGripOffset non caricata" );
          return false;
      }
      if ( !nh_.getParam ( "offset_positions/cabinaAttachOffset", cabina_attach_offset_vec_ ) )
      {
          ROS_ERROR ( "posizione panelGripOffset non caricata" );
          return false;
      }
      if ( !nh_.getParam ( "offset_positions/cabinaOffset", cabina_offset_vec_ ) )
      {
          ROS_ERROR ( "posizione cabinaoffset non caricata" );
          return false;
      }
      if ( !nh_.getParam ( "offset_positions/stiima_cabinaOffset", stiima_cabina_offset_vec_ ) )
      {
          ROS_WARN ( "posizione stiima_cabinaoffset non caricata, quale fusoliera stai usando ?" );
//           return ;
      }
      
      alpha_ = 0.1;
      if ( !nh_.getParam ( "/params/alphaFus", alpha_ ) )
          ROS_INFO ( "numero alphaFus non caricato . Using default: %f", alpha_);
      
      default_velocity_ = 0.5;
      if ( !nh_.getParam ( "default_velocity", default_velocity_ ) )
          ROS_INFO ( "default_velocity not found on rosparam, using default value: 0.5" );
      
      panel_velocity_ = 0.1;
      if ( !nh_.getParam ( "panel_velocity", panel_velocity_ ) )
          ROS_INFO ( "panel_velocity not found on rosparam, using default value: 0.01" );
      
      vecToTf ( &panel_offset_vec_, cart_TO_sidePanel_ );
      
      
      calito_fuselage_ = req.CALITO_fuselage;
      std::string fus  = req.CALITO_fuselage == true ? "CALITO " : "STIIMA";
      std::cout<<"calito fuselage from service param: "<< fus <<std::endl;
      
      fus  = calito_fuselage_ == true ? "CALITO " : "STIIMA";
      std::cout<<"calito fiselage variable param: "<< fus <<std::endl;
      
      if( scene_configurated_ && !req.configure_scene)
      {
          ROS_WARN ( "The scene is already configured. Return True." );
          return (res.success = false);
      }
      
      std::string ss = calito_fuselage_ == true ? "true " : "false";
      std::cout<<"calito fuselage: "<< ss <<std::endl;
      
      if(calito_fuselage_){
        vecToTf ( &cabina_offset_vec_,attachPoint_TO_fuselageOrigin_ );
      }
      else{
        vecToTf ( &stiima_cabina_offset_vec_,attachPoint_TO_fuselageOrigin_ );
        PANEL_PATH = STIIMA_PANEL_PATH;
        FUSELAGE_PATH = STIIMA_FUSELAGE_PATH;
      }
      const int max_random_seed = 200;

      move_group_.detachObject ( PANEL_ID);
      ros::Duration ( 1.0 ).sleep();
      planning_scene_interface_.removeCollisionObjects ( object_ids_ );
      
      setRobotState(joint_home_);
      
      std::cout << BOLDMAGENTA << "****** Configure Scene and Models ******** " << RESET << std::endl;
      
      tf::Pose panel_pose, assembly_pose, cart_pose, fuselage_poseWRTworld;
      tf::poseMsgToTF ( req.panelPose, panel_pose );
      tf::poseMsgToTF ( req.assemblyPose, assembly_pose );
      
      std::cout << BOLDCYAN << " Update date from /tf" << RESET << std::endl;
      tf::TransformListener listener;
      listener.waitForTransform ( "/world", BASE_LINK, ros::Time ( 0 ), ros::Duration ( 10.0 ) );
      listener.lookupTransform ( "/world",  BASE_LINK, ros::Time ( 0 ), worldToRobot_ );
      
      tf::Pose panelPose_tf  = worldToRobot_ * panel_pose;
      tf::Pose attachPose_tf = worldToRobot_ * assembly_pose;
      
      geometry_msgs::Pose w2r;
      tf::poseTFToMsg(worldToRobot_,w2r);
      std::cout << BOLDBLUE << "World to robot tf" << w2r << RESET << std::endl;
      
      cart_pose = panel_pose * cart_TO_sidePanel_.inverse();
      
      
      fuselage_poseWRTworld = assembly_pose * attachPoint_TO_fuselageOrigin_;
      
      std::cout << BOLDCYAN << " Add Collision Objects" << RESET << std::endl;
      moveit_msgs::CollisionObject collision_object_panel_2;
      addObject ( CART_ID, CART_PATH, BASE_LINK, &cart_pose, &collision_object_cart_ );
      
      //fine
      
      addObject ( PANEL_ID, PANEL_PATH, BASE_LINK, &panel_pose, &collision_object_panel_ );
      
      addObject ( FUSELAGE_ID,FUSELAGE_PATH, BASE_LINK, &fuselage_poseWRTworld, &collision_object_cabina_ );
      
      object_ids_.push_back ( collision_object_panel_.id );
      
      //--------------------------- verifico che non ci siano collisioni--------------------------
      cov_.clear();
      cov_.push_back ( collision_object_panel_ );
//       cov_.push_back ( collision_object_panel_2 );
      cov_.push_back ( collision_object_cart_   );
      cov_.push_back ( collision_object_cabina_ );
      
      std::cout << BOLDCYAN << " Create Robot MOdel and IK solver" << RESET << std::endl;
      robot_model_ = ( new robot_model_loader::RobotModelLoader ( "robot_description" ) )->getModel();
      solver_ = robot_model_->getJointModelGroup ( PLANNING_GROUP )->getSolverInstance();
      boost::shared_ptr<descartes_moveit::IkFastMoveitStateAdapter> iiwa_model ( new descartes_moveit::IkFastMoveitStateAdapter );
      // boost::shared_ptr<descartes_moveit::MoveitStateAdapter> iiwa_model ( new descartes_moveit::MoveitStateAdapter );
      nh_.setParam ( PLANNING_GROUP + "/ikfast_base_frame",BASE_LINK );
      nh_.setParam ( PLANNING_GROUP + "/ikfast_tool_frame",GRIP_ENDEFFECTOR );

      if ( !iiwa_model_->initialize ( "robot_description", PLANNING_GROUP, BASE_LINK, GRIP_ENDEFFECTOR ) )
      {
          ROS_INFO ( "Could not initialize robot model" );
          return false;
      }
      iiwa_model_->setCheckCollisions ( false );

      std::cout << BOLDCYAN << " Create the motion planner" << RESET << std::endl;
      descartes_planner::DensePlanner iiwa_planner;
      iiwa_planner_.initialize ( ( descartes_core::RobotModelPtr ) iiwa_model_ );


      if ( !applyAndCheckPS ( nh_, cov_, robot_model_, alpha_ ) )
      {
        noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
        res.success = false;
        return true;
      }
      
      robot_model_loader::RobotModelLoader robot_model_loader ( "robot_description" );
      robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
      planning_scene::PlanningScene planning_scene ( kinematic_model );
      collision_detection::AllowedCollisionMatrix acm = planning_scene.getAllowedCollisionMatrix();
      
      acm.setEntry(GRIP_ENDEFFECTOR,PANEL_ID,false);
      
      moveit_msgs::AllowedCollisionMatrix acm_msg;
      acm.getMessage(acm_msg);
      ros::Publisher planning_scene_diff_publisher = nh_.advertise<moveit_msgs::PlanningScene> ( "planning_scene", 1 );
      while ( planning_scene_diff_publisher.getNumSubscribers() < 1 )
      {
          ros::WallDuration sleep_t ( 0.5 );
          sleep_t.sleep();
      }
      moveit_msgs::PlanningScene planning_scene_msg;
      planning_scene_msg.is_diff = true;
      planning_scene_msg.allowed_collision_matrix = acm_msg;
      
      planning_scene_diff_publisher.publish ( planning_scene_msg );
      ros::Duration ( 1.0 ).sleep();
      getPlanningScene ( nh_, kinematic_model, "", planning_scene );
      
      std::cout << BOLDMAGENTA << "****** Configure Scene and Models OK ******** " << RESET << std::endl;
      
      return (scene_configurated_ = res.success = true);
    }
    
    /**
     * planToTarget
     */
    bool planToTarget ( eureca_assembly_msgs::planConnectionRequest&  req
                      , eureca_assembly_msgs::planConnectionResponse&  res )
    {
      std::cout << BOLDMAGENTA << "****** Plan To Target ******** " << RESET << std::endl;
      if( !scene_configurated_ )
      {
        ROS_ERROR("The scene has not been yet configured");
        res.success = false;
        return true;
      }
      
      move_group_.setMaxVelocityScalingFactor(default_velocity_);
      ROS_INFO("Position of the robot available");
      
      moveit_msgs::RobotState start_state;
      start_state.joint_state = req.start_configuration;
      move_group_.setStartState(start_state);
      move_group_.setJointValueTarget( req.target_configuration );
      
      if ( ! ( move_group_.plan ( my_plan_  ) == moveit::planning_interface::MoveItErrorCode::SUCCESS ) )
      {
        ROS_ERROR("pianificazione verso posa corrente non riuscita, fanchiulo");
        ROS_ERROR_STREAM("Start State:" << start_state );
        ROS_ERROR_STREAM("Target State:" << req.target_configuration );
        std::cout<<BOLDRED<<"\n\n\nnon ci siamo, non riesco a pianificare neanche se mi ammazzano\n\n\n"<<RESET<<std::endl;
        return (res.success = false);
      }
      res.trajectory = my_plan_.trajectory_.joint_trajectory;
      std::cout << BOLDMAGENTA << "****** Plan To Target OK ******** " << RESET << std::endl;
      
      return (res.success = true);
    }
    /**
     * planToTarget - END
     */
        /**
     * planCorrection
     */
    bool planCorrection ( eureca_assembly_msgs::planCorrectionRequest&  req
                        , eureca_assembly_msgs::planCorrectionResponse&  res )
    {
      std::cout << BOLDMAGENTA << "****** Plan correction wrt nominal pose ******** " << RESET << std::endl;
      if( !scene_configurated_ )
      {
        ROS_ERROR("The scene has not been yet configured");
        res.success = false;
        return true;
      }

      ROS_INFO("Position of the robot available");

      if( !descartes(req.nominal_pose, req.actual_pose, my_plan_, req.starting_joint_values ) ) 
        {
          ROS_ERROR("Error in movement execution simulation. Why?");
          
        }
      
      res.trajectory = my_plan_.trajectory_.joint_trajectory;
      
      std::cout << BOLDMAGENTA << "****** Plan To Target OK ******** " << RESET << std::endl;
      
      return (res.success = true);
    }
    /**
     * planCorrection - END
     */

    /**
     * planCallback
     */
    bool planCallback ( eureca_assembly_msgs::planAssemblyRequest  &req
                      , eureca_assembly_msgs::planAssemblyResponse &res )
    {
      std::string rec = req.configure_scene == true ? "true" : "false";
      std::cout<<"reconfigure scene: "<< rec <<std::endl;
      
      calito_fuselage_ = req.CALITO_fuselage;
      std::string fus  = req.CALITO_fuselage == true ? "CALITO " : "STIIMA";
      std::cout<<"calito fuselage from service param: "<< fus <<std::endl;
      fus  = calito_fuselage_ == true ? "CALITO " : "STIIMA";
      std::cout<<"calito fiselage variable param: "<< fus <<std::endl;
      
      if( !scene_configurated_ || req.configure_scene )
      {
        std::cout<<"reconfig"<<std::endl;
        if( ! configurationCallback  ( req, res ) )
        {
          ROS_ERROR("Uffa, la configurazione non e andata");
          noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
          res.success = false;
          return true;
        }
      }
      
      std::cout << BOLDMAGENTA << "****** Plan assembly ******** " << RESET << std::endl;

//------------------------trasformo le pose in TF e calcolo le posizioni ---------------------------------------
      double planning_ok = false;
      
      int START_TO_HOME        = 0;
      int HOME_TO_APPROACH     = 1;
      int APPROACH_TO_GRIPPING = 2;
      int GRIPPING_TO_ESCAPE   = 3;
      int ESCAPE_TO_ATTACH     = 4;
      int ATTACH_TO_AWAY       = 5;
      int AWAY_TO_HOME         = 6;
      
      execute_plan_motion_ = true;//req.execute_plan_motion;
      
      std::vector<std::string> traj_ids = {"START_TO_HOME"
                                          ,"HOME_TO_APPROACH"
                                          ,"APPROACH_TO_GRIPPING"
                                          ,"GRIPPING_TO_ESCAPE" 
                                          ,"ESCAPE_TO_ATTACH" 
                                          ,"ATTACH_TO_AWAY" 
                                          ,"AWAY_TO_HOME"
                                          };
      int n_pose = traj_ids.size();
      
      std::vector<std::vector<double>> target_joint(n_pose, std::vector<double>(7,0)), starting_joint(n_pose, std::vector<double>(7,0));
      std::vector<bool> calc_traj(n_pose,true);
      
      robot_state::RobotStatePtr          robot_state(new robot_state::RobotState(robot_model_));
      const robot_state::JointModelGroup* joint_model_group = robot_model_->getJointModelGroup("manipulator");
      robot_state->setJointGroupPositions(joint_model_group, joint_home_);
      const Eigen::Affine3d& home_tf = robot_state->getGlobalLinkTransform(GRIP_ENDEFFECTOR);
      geometry_msgs::Pose home_pose;
      tf::poseEigenToMsg (home_tf, home_pose);                                                                  // home
      

      geometry_msgs::Pose panel_origin_pose   = req.panelPose;                                                      //2
      geometry_msgs::Pose gripping_pose;                                                      //2
      geometry_msgs::Pose attach_pose         = req.assemblyPose;                                                   //4
      
      
      
            
      tf::Pose attach_pose_tf, offset_attach;
      tf::poseMsgToTF(attach_pose,attach_pose_tf);
      
      vecToTf(&panel_grip_offset_vec_,offset_attach);
      
      attach_pose_tf = attach_pose_tf * offset_attach;
      
      tf::poseTFToMsg(attach_pose_tf, attach_pose);
      
      
      
      
      if ( !solver_->getPositionIK ( attach_pose, std::vector<double>(7,0), target_joint[ ESCAPE_TO_ATTACH ], error_code_ ) ){
        geometry_msgs::PoseStamped cp = move_group_.getCurrentPose(BASE_LINK);
        std::cout<<"attach_pose pose: " << attach_pose <<std::endl;
        std::cout<<"base link pose: " << cp           <<std::endl;
        ROS_ERROR("getPositionIK failed for attach pose");
        noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
        
        ros::Publisher pub = nh_.advertise<geometry_msgs::PoseStamped>("/plan_alert", 1000);
        geometry_msgs::PoseStamped p;
        p.pose.position.x = -2;
        pub.publish(p);
        
        res.success = false;
        return true;
      }      
      if( !setRobotState( target_joint[ ESCAPE_TO_ATTACH ] ))
      {
        ROS_ERROR("non è nello stato corretto !!");
      }
      
//       std::cin.get();
      
      geometry_msgs::Pose check_attach_pose;  
      tf::Pose ck_pose_tf, ck_offset, att_tf;
      
      tf::poseMsgToTF(attach_pose,att_tf);
      
      tf::Vector3 oa;
      oa.setX(0);
      oa.setY(0);
      oa.setZ(-0.1);
      
      double r = 0, p = 0, y = 0;
      
      tf::Quaternion qa = tf::createQuaternionFromRPY( r, p, y);
      
      ck_offset.setOrigin(oa);
      ck_offset.setRotation(qa);
      
      ck_pose_tf = att_tf * ck_offset;
      tf::poseTFToMsg(ck_pose_tf,check_attach_pose);
      
      std::vector<double> vec ( 7,0 );
      
      
      
      
      
      if ( !solver_->getPositionIK ( check_attach_pose, std::vector<double>(7,0), vec, error_code_ ) ){
        ROS_ERROR("getPositionIK failed for check pose");
        
        ros::Publisher pub = nh_.advertise<geometry_msgs::PoseStamped>("/plan_alert", 1000);
        geometry_msgs::PoseStamped p;
        p.pose.position.x = -2;
        pub.publish(p);
        
        res.success = false;
        return true;
      }
      
      if( !setRobotState( vec ))
      {
        ROS_ERROR("non è nello stato corretto !!");
      }
      
      
//       while(ros::ok()){
      
        robot_state->setJointGroupPositions(joint_model_group,vec);
        
        Eigen::Vector3d reference_point_position(0.0,0.0,0.0);
        Eigen::MatrixXd jacobian;
        robot_state->getJacobian(joint_model_group, robot_state->getLinkModel(GRIP_ENDEFFECTOR),
                                    reference_point_position,
                                    jacobian);
        ROS_INFO_STREAM("Jacobian: " << jacobian);
        
        kinematics::KinematicsQueryOptions options;
        options.lock_redundant_joints = true;
        
//         robot_state->setFromIK();
        
//         std::cin.get();
//       }
      
      
      tf::Pose panel_origin_pose_tf, gripping_pose_tf, offset_grip;
      tf::poseMsgToTF(panel_origin_pose,panel_origin_pose_tf);
      
      vecToTf(&panel_grip_offset_vec_,offset_grip);
      
      gripping_pose_tf = panel_origin_pose_tf * offset_grip;
      
      tf::poseTFToMsg(gripping_pose_tf, gripping_pose);
      
      std::cout<< "offsetvec: "<< std::endl;
      for (auto d : panel_grip_offset_vec_)
        std::cout<< d <<std::endl;
      
      std::cout << "gripping_pose: "<< gripping_pose << std::endl;
      
      // --- approach, first IK solution TODO:: come parametro!
      geometry_msgs::Pose approach_pose;  
      tf::Pose approach_pose_tf, approach_offset;
      
      tf::Vector3 o;
      o.setX(0);
      o.setY(0);
      o.setZ(0.05);
      tf::Quaternion q;
      q.setW(1);
      
      approach_offset.setOrigin(o);
      approach_offset.setRotation(q);
      
      approach_pose_tf = gripping_pose_tf * approach_offset;
      tf::poseTFToMsg(approach_pose_tf,approach_pose);
      
      if ( !solver_->getPositionIK ( gripping_pose, std::vector<double>(7,0), target_joint[ APPROACH_TO_GRIPPING ], error_code_ ) ){
        geometry_msgs::PoseStamped cp = move_group_.getCurrentPose(BASE_LINK);
        std::cout<<"gripping pose: " << gripping_pose<<std::endl;
        std::cout<<"base link pose: " << cp           <<std::endl;
        ROS_ERROR("getPositionIK failed for gripping pose");
        noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
        
        ros::Publisher pub = nh_.advertise<geometry_msgs::PoseStamped>("/plan_alert", 1000);
        geometry_msgs::PoseStamped p;
        p.pose.position.x = -1;
        pub.publish(p);
        
        res.success = false;
        return true;
      }
      
      if( !setRobotState( target_joint[ APPROACH_TO_GRIPPING ] ))
        ROS_ERROR("non è nello stato corretto !!");
      
      std::cout<<BOLDBLUE<<"\n\napproac pose: "<< approach_pose << RESET<<std::endl;
      
      if ( !solver_->getPositionIK ( approach_pose, std::vector<double>(7,0), target_joint[ HOME_TO_APPROACH], error_code_ ) ){
        ROS_ERROR("getPositionIK failed for approac pose");
        
        ros::Publisher pub = nh_.advertise<geometry_msgs::PoseStamped>("/plan_alert", 1000);
        geometry_msgs::PoseStamped p;
        p.pose.position.x = -1;
        pub.publish(p);
        
        res.success = false;
        return true;
      }
      geometry_msgs::Pose escape_pose;    escape_pose   = gripping_pose; escape_pose.position.z   += 0.05;// escape_pose.position.y   += 0.1;      //3
      geometry_msgs::Pose away_pose;      away_pose     = attach_pose;   away_pose.position.x     -= 0.01;      //5
      
      /**
       * 
       */
      move_group_.setMaxVelocityScalingFactor(default_velocity_);
      std::cout << BOLDCYAN << " From Actual State to Home" << RESET << std::endl;
      move_group_.setStartStateToCurrentState();
      move_group_.setJointValueTarget( js_ );

      if( !setRobotState( joint_home_ ))
      {
        ROS_ERROR("non è nello stato corretto !!");
      }
      
      
      
      
      
      /**
       * 
       */
      std::vector<std::string> links, unlinks;
      links.push_back(GRIP_ENDEFFECTOR);
      links.push_back("olivia_link_6");
      links.push_back("olivia_link_7");
      links.push_back("olivia_link_ee");
      links.push_back("Gripper");
      links.push_back("Gripper_ee_link");
      links.push_back("Gripper_base_link");
      
      unlinks.push_back(FUSELAGE_ID);
      /**
       * 
       */   
      std::vector<moveit::planning_interface::MoveGroupInterface::Plan> plans_vec(n_pose);
      bool end_planning = false;
      int cnt = 0;
      
      double start_time = ros::Time::now().toSec();
      std::cout << BOLDBLUE << "Start planning " << RESET << std::endl;
      
      while( ! end_planning )
      {
        std::cout << "\n TRIAL " << cnt++ << "\n" << std::endl;
        std::cout << BOLDCYAN << " POS -----> HOME" << RESET << std::endl;
        
        move_group_.setStartStateToCurrentState();
      
        move_group_.setJointValueTarget ( joint_home_ );
        
        if( calc_traj[ START_TO_HOME ] ){
          if ( ! ( move_group_.plan ( my_plan_  ) == moveit::planning_interface::MoveItErrorCode::SUCCESS ) )
            ROS_ERROR("error plannig to home position");
          plans_vec [ START_TO_HOME ] = my_plan_;
          starting_joint[ START_TO_HOME ] = plans_vec[START_TO_HOME].trajectory_.joint_trajectory.points.front().positions;
          target_joint  [ START_TO_HOME ] = plans_vec[START_TO_HOME].trajectory_.joint_trajectory.points.back().positions;
          std::cout << BOLDMAGENTA <<"First Traj Point: " << RESET << to_string( starting_joint[ START_TO_HOME ] ) << std::endl;
          std::cout << BOLDMAGENTA <<"Last  Traj Point: " << RESET << to_string( target_joint[ START_TO_HOME ] )   << std::endl;
          calc_traj     [ START_TO_HOME ] = false;
        }
        
        if(execute_plan_motion_){
          move_group_.execute(plans_vec [ START_TO_HOME ]);
        }else{
          if( !setRobotState( target_joint  [ START_TO_HOME ] ))
          {
            ROS_ERROR("non è nello stato corretto !!");
          }
        }
        
//-------------------------------------RRT from home to approach ---------------------------------------------
        std::cout << BOLDCYAN <<" HOME -----> APROACH: calc? " << calc_traj[ HOME_TO_APPROACH ] << RESET << std::endl;
        if( calc_traj[ HOME_TO_APPROACH ] )
        {
          
//           allowPanelCollisions(links,   PANEL_ID, false);
//           allowPanelCollisions(unlinks, PANEL_ID, true);
          
          starting_joint[  HOME_TO_APPROACH ] = (move_group_.getCurrentJointValues());
          std::cout << BOLDMAGENTA <<"Starting pos: " << RESET << to_string( starting_joint[ HOME_TO_APPROACH ] ) << std::endl;
          if( !RRTplan( target_joint[ HOME_TO_APPROACH], plans_vec[HOME_TO_APPROACH] ) )
          {
            ROS_ERROR("HOME TO APPROACH: RRTplan failed! Abort?");
            noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
          }
          starting_joint[ HOME_TO_APPROACH ] = plans_vec[HOME_TO_APPROACH].trajectory_.joint_trajectory.points.front().positions;
          target_joint  [ HOME_TO_APPROACH ] = plans_vec[HOME_TO_APPROACH].trajectory_.joint_trajectory.points.back().positions;
          calc_traj     [ HOME_TO_APPROACH ] = false;
          std::cout << BOLDMAGENTA <<"First Traj Point: " << RESET << to_string( starting_joint[ HOME_TO_APPROACH ] ) << std::endl;
          std::cout << BOLDMAGENTA <<"Last  Traj Point: " << RESET << to_string( target_joint[ HOME_TO_APPROACH ] )   << std::endl;
        }
        
        std::cout << BOLDGREEN <<" HOME -----> APROACH: move " << std::endl;
        
        if(execute_plan_motion_){
          move_group_.execute(plans_vec [ HOME_TO_APPROACH ]);
        }else{
          if( !setRobotState( target_joint  [ HOME_TO_APPROACH ] ))
          {
            ROS_ERROR("non è nello stato corretto !!");
          }
        }
        
        
// #if 0
//-------------------------------------descartes---------------------------------------------
        std::cout << BOLDCYAN << " APPROACH -----> GRIPPING: calc? " << calc_traj[ APPROACH_TO_GRIPPING ]  << RESET << std::endl;
        if( calc_traj[ APPROACH_TO_GRIPPING ] )
        {
          
//           allowPanelCollisions(links,   PANEL_ID, true);
//           allowPanelCollisions(unlinks, PANEL_ID, false);
          
          if(! descartes(approach_pose, gripping_pose, plans_vec[ APPROACH_TO_GRIPPING ]))
          {
            ROS_ERROR("descartes fail!  Abort?");
            noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
            res.success = false;
            return true;
          }
          starting_joint[ APPROACH_TO_GRIPPING ] = plans_vec[ APPROACH_TO_GRIPPING ].trajectory_.joint_trajectory.points.front().positions;
          target_joint  [ APPROACH_TO_GRIPPING ] = plans_vec[ APPROACH_TO_GRIPPING ].trajectory_.joint_trajectory.points.back().positions;
          calc_traj     [ APPROACH_TO_GRIPPING ] = false;

          std::cout << BOLDMAGENTA <<"First Traj Point: " << RESET << to_string( starting_joint[ APPROACH_TO_GRIPPING ] ) << std::endl;
          std::cout << BOLDMAGENTA <<"Last  Traj Point: " << RESET << to_string( target_joint[ APPROACH_TO_GRIPPING ] )   << std::endl;
          
          if( !std::equal( target_joint[HOME_TO_APPROACH].begin(), target_joint[HOME_TO_APPROACH].end()
                          , starting_joint[ APPROACH_TO_GRIPPING ].begin(), [](double j, double q) { return std::fabs(j-q)<0.001; }) )
          {
              target_joint[ HOME_TO_APPROACH ] = starting_joint[ APPROACH_TO_GRIPPING ];
              calc_traj   [ HOME_TO_APPROACH ] = true;
              std::cout << BOLDRED << " APPROACH -----> GRIPPING: mismatch with prev path. Recalc. "<< RESET << std::endl;
              continue;
          }
        }
        
        
        if(execute_plan_motion_)
        {
          move_group_.execute(plans_vec [ APPROACH_TO_GRIPPING ]);
        }
        else
        {
          if( !setRobotState( target_joint  [ APPROACH_TO_GRIPPING ] ))
          {
            ROS_ERROR("non è nello stato corretto !!");
          }
        }
        move_group_.attachObject ( collision_object_panel_.id,GRIP_ENDEFFECTOR );
        
        
//----------------------------------------descartes------------------------------------------
        std::cout << BOLDCYAN << " GRIPPING -----> ESCAPE: calc? " << calc_traj[ GRIPPING_TO_ESCAPE ]  << RESET << std::endl;
        if( calc_traj[ GRIPPING_TO_ESCAPE ] )
        {
//           allowPanelCollisions(links,   PANEL_ID, true);
//           allowPanelCollisions(unlinks, PANEL_ID, false);
          
          starting_joint[ GRIPPING_TO_ESCAPE ] = target_joint[ APPROACH_TO_GRIPPING ];
          std::cout << BOLDMAGENTA <<"Starting Point: " << RESET << to_string( starting_joint[ GRIPPING_TO_ESCAPE ] ) << std::endl;
          if( !descartes(gripping_pose, escape_pose, plans_vec[ GRIPPING_TO_ESCAPE ],  starting_joint[ GRIPPING_TO_ESCAPE ] ) ) 
          {
            ROS_ERROR("descartes fail . Why?");
            noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
            res.success = false;
            return true;
            
          }
          starting_joint[ GRIPPING_TO_ESCAPE ] = plans_vec[ GRIPPING_TO_ESCAPE ].trajectory_.joint_trajectory.points.front().positions;
          target_joint  [ GRIPPING_TO_ESCAPE ] = plans_vec[ GRIPPING_TO_ESCAPE ].trajectory_.joint_trajectory.points.back().positions;
          calc_traj     [ GRIPPING_TO_ESCAPE ] = false;
          
          std::cout << BOLDMAGENTA <<"First Traj Point: " << RESET << to_string( starting_joint[ GRIPPING_TO_ESCAPE ] ) << std::endl;
          std::cout << BOLDMAGENTA <<"Last  Traj Point: " << RESET << to_string( target_joint[ GRIPPING_TO_ESCAPE ] )   << std::endl;
            
        }
        
        
        std::cout << BOLDGREEN << " GRIPPING -----> ESCAPE: move " << std::endl;
        
        if(execute_plan_motion_){
          move_group_.execute(plans_vec [ GRIPPING_TO_ESCAPE ]);
        }else{
          if( !setRobotState( target_joint  [ GRIPPING_TO_ESCAPE ] ))
          {
            ROS_ERROR("non è nello stato corretto !!");
          }
        }
// #if 0
//----------------------slow down while moveing the panel-----------------------------------------------------------------------------------------------------
         move_group_.setMaxVelocityScalingFactor(panel_velocity_);
//----------------------------------------RRT------------------------------------------
        std::cout << BOLDCYAN << " ESCAPE -----> ATTACH: calc? " << calc_traj[ ESCAPE_TO_ATTACH ]  << RESET << std::endl;
        if( calc_traj[ ESCAPE_TO_ATTACH ] )
        {        
          robot_model_loader::RobotModelLoader robot_model_loader ( "robot_description" );
          robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
          planning_scene::PlanningScene planning_scene ( kinematic_model );
          collision_detection::AllowedCollisionMatrix acm = planning_scene.getAllowedCollisionMatrix();
          
          acm.setEntry(GRIP_ENDEFFECTOR,PANEL_ID,true);
          acm.setEntry("olivia_link_6",PANEL_ID,true);
          acm.setEntry("olivia_link_7",PANEL_ID,true);
          acm.setEntry("olivia_link_ee",PANEL_ID,true);
          acm.setEntry("Gripper",PANEL_ID,true);
          acm.setEntry("Gripper_ee_link",PANEL_ID,true);
          acm.setEntry("Gripper_base_link",PANEL_ID,true);
          acm.setEntry(FUSELAGE_ID,PANEL_ID,false);
          
          moveit_msgs::AllowedCollisionMatrix acm_msg;
          acm.getMessage(acm_msg);
          
          ros::Publisher planning_scene_diff_publisher = nh_.advertise<moveit_msgs::PlanningScene> ( "planning_scene", 1 );
          while ( planning_scene_diff_publisher.getNumSubscribers() < 1 )
          {
              ros::WallDuration sleep_t ( 0.5 );
              sleep_t.sleep();
          }
          moveit_msgs::PlanningScene planning_scene_msg;
          
          planning_scene_msg.is_diff = true;
          planning_scene_msg.allowed_collision_matrix = acm_msg;
          
          planning_scene_diff_publisher.publish ( planning_scene_msg );
          ros::Duration ( 1.0 ).sleep();
          
          getPlanningScene ( nh_, kinematic_model, "", planning_scene );
          
//           allowPanelCollisions(links,   PANEL_ID, true);
//           allowPanelCollisions(unlinks, PANEL_ID, false);
          
          starting_joint[ ESCAPE_TO_ATTACH ] = (move_group_.getCurrentJointValues());            
          std::cout << BOLDMAGENTA <<"Starting Point: " << RESET << to_string( starting_joint[ ESCAPE_TO_ATTACH ] ) << std::endl;
          if( !RRTplan( target_joint[ESCAPE_TO_ATTACH ], plans_vec[ ESCAPE_TO_ATTACH ]) )
          {
            ROS_ERROR("Failed in planning from ESCAPE TO ATTACH");
            noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
          }
          
          starting_joint[ ESCAPE_TO_ATTACH ] = plans_vec[ ESCAPE_TO_ATTACH ].trajectory_.joint_trajectory.points.front().positions;
          target_joint  [ ESCAPE_TO_ATTACH ] = plans_vec[ ESCAPE_TO_ATTACH ].trajectory_.joint_trajectory.points.back().positions;
          calc_traj     [ ESCAPE_TO_ATTACH ] = false;
          std::cout << BOLDMAGENTA <<"First Traj Point: " << RESET << to_string( starting_joint[ ESCAPE_TO_ATTACH ] ) << std::endl;
          std::cout << BOLDMAGENTA <<"Last  Traj Point: " << RESET << to_string( target_joint[ ESCAPE_TO_ATTACH ] )   << std::endl;
          
        }
        
        std::cout << BOLDGREEN << " ESCAPE -----> ATTACH: move " << std::endl;        
        
        if(execute_plan_motion_){
          move_group_.execute(plans_vec [ ESCAPE_TO_ATTACH ]);
        }else{
          if( !setRobotState( target_joint  [ ESCAPE_TO_ATTACH ] ))
          {
            ROS_ERROR("non è nello stato corretto !!");
          }
        }
        
        allowPanelCollisions(links,PANEL_ID,false);
        move_group_.detachObject ( collision_object_panel_.id );
        
        move_group_.setMaxVelocityScalingFactor(default_velocity_);
//---------------------------------------descartes-------------------------------------------
        std::cout << BOLDCYAN << " ATTACH -----> AWAY: calc? " << calc_traj[ ATTACH_TO_AWAY ]  << RESET << std::endl;
        if( calc_traj[ ATTACH_TO_AWAY ] )
        {
//           allowPanelCollisions(links,   PANEL_ID, true);
//           allowPanelCollisions(unlinks, PANEL_ID, false);
          
          starting_joint[ ATTACH_TO_AWAY ] = (move_group_.getCurrentJointValues());
          std::cout << BOLDMAGENTA <<"Starting Point: " << RESET << to_string( starting_joint[ ATTACH_TO_AWAY ] ) << std::endl;
          if( !descartes(attach_pose, away_pose, plans_vec[ ATTACH_TO_AWAY ]))
          {
            ROS_ERROR("Failed in planning from ATTACH TO AWAY");
            noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
            res.success = false;
            return true;
          }
          starting_joint[ ATTACH_TO_AWAY ] = plans_vec[ ATTACH_TO_AWAY ].trajectory_.joint_trajectory.points.front().positions;
          target_joint  [ ATTACH_TO_AWAY ] = plans_vec[ ATTACH_TO_AWAY ].trajectory_.joint_trajectory.points.back().positions;
          calc_traj     [ ATTACH_TO_AWAY ] = false;
          
          std::cout << BOLDMAGENTA <<"First Traj Point: " << RESET << to_string( starting_joint[ ATTACH_TO_AWAY ] ) << std::endl;
          std::cout << BOLDMAGENTA <<"Last  Traj Point: " << RESET << to_string( target_joint[ ATTACH_TO_AWAY ] )   << std::endl;

          
            if( !std::equal( target_joint[ESCAPE_TO_ATTACH].begin(), target_joint[ESCAPE_TO_ATTACH].end()
                          , starting_joint[ ATTACH_TO_AWAY ].begin(), [](double j, double q) { return std::fabs(j-q)<0.001; }) )
          {
              target_joint[ ESCAPE_TO_ATTACH ] = starting_joint[ ATTACH_TO_AWAY ];
              calc_traj   [ ESCAPE_TO_ATTACH ] = true;
              std::cout << " ATTACH -----> AWAY: mismatch with prev path. Recalc. " << std::endl;
        
              noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
              if ( !applyAndCheckPS ( nh_, cov_, robot_model_, alpha_ ) )
              {
                ROS_ERROR("Uffa");
                noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
                res.success = false;
                return true;
              }
              continue;
          }
        }
        
        std::cout << BOLDGREEN << " ATTACH -----> AWAY: move "  << RESET << std::endl;
        
        if(execute_plan_motion_){
          move_group_.execute(plans_vec [ ATTACH_TO_AWAY ]);
        }else{
          if( !setRobotState( target_joint  [ ATTACH_TO_AWAY ] ))
          {
            ROS_ERROR("non è nello stato corretto !!");
          }
        }
        
// #endif
        end_planning = true;
      }
      
//-----------------------------------------------------------------------------------------------------------------
// #if 0
      ROS_INFO ( "return to home position" );
      
      move_group_.setStartStateToCurrentState();
      
      move_group_.setJointValueTarget ( joint_home_ );
      
      if ( ! ( move_group_.plan ( plans_vec [ AWAY_TO_HOME ]  ) == moveit::planning_interface::MoveItErrorCode::SUCCESS ) )
      {
        noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
        res.success = false;
        return true; 
      }
    
      if(execute_plan_motion_)
      {
        move_group_.execute(plans_vec [ AWAY_TO_HOME ]);
      }
      else
      {
        if( !setRobotState( target_joint  [ AWAY_TO_HOME ] ))
        {
          ROS_ERROR("non è nello stato corretto !!");
        }
      }
      
      starting_joint[ AWAY_TO_HOME ] = plans_vec[AWAY_TO_HOME].trajectory_.joint_trajectory.points.front().positions;
      target_joint  [ AWAY_TO_HOME ] = plans_vec[AWAY_TO_HOME].trajectory_.joint_trajectory.points.back().positions;
      calc_traj     [ AWAY_TO_HOME ] = false;
      std::cout << BOLDMAGENTA <<"First Traj Point: " << RESET << to_string( starting_joint[ AWAY_TO_HOME ] ) << std::endl;
      std::cout << BOLDMAGENTA <<"Last  Traj Point: " << RESET << to_string( target_joint[ AWAY_TO_HOME ] )   << std::endl;
      
      move_group_.detachObject ( PANEL_ID);
      ros::Duration ( 1.0 ).sleep();
      planning_scene_interface_.removeCollisionObjects ( object_ids_ );
      
      if ( !applyAndCheckPS ( nh_, cov_, robot_model_, alpha_ ) )
      {
        ROS_ERROR("Uffa");
        noSuccess ( &move_group_, &collision_object_panel_, &planning_scene_interface_, &object_ids_ );
        res.success = false;
        return true;
      }
      
// #endif
      
      std::cout << BOLDBLUE << "total planning time: " << ros::Time::now().toSec() - start_time << RESET << std::endl;
      
      std::cout << BOLDBLUE <<"Writing on rosparam " << RESET << std::endl;
  
      nh_.deleteParam("/traj_ids");
      nh_.deleteParam("/trajectories_planned");
      nh_.setParam("/traj_ids", traj_ids);
      
      
      for(int i=0;i< plans_vec.size();i++){
        
        std::string path = "/trajectories_planned/"+traj_ids[i];
        
        if(! trajectory_processing::setTrajectoryToParam(nh_, path, plans_vec[i].trajectory_.joint_trajectory))
          ROS_ERROR("%s not set on ros param",traj_ids[i].c_str());
        
      }
      
      std::cout << BOLDBLUE <<"Wrote, done!"  << std::endl;
      
      planning_scene_interface_.removeCollisionObjects ( object_ids_ );
      
      std::cout<<"FINISH"<<std::endl;
//       std::cin.get();
      
      res.success = true;
      
      return true;
    }
    
    
};

int main ( int argc, char **argv )
{
    ros::init ( argc, argv, "plan_assembly" );
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner ( 4 );
    spinner.start();

    planner p ( nh );

    ros::ServiceServer configure = nh.advertiseService ( "configure_assembly_scene" , &planner::configurationCallback , &p );
    ros::ServiceServer service   = nh.advertiseService ( "plan_assembly"            , &planner::planCallback          , &p );
    ros::ServiceServer connect   = nh.advertiseService ( "plan_connection"          , &planner::planToTarget          , &p );
    ros::ServiceServer correct   = nh.advertiseService ( "plan_correction"          , &planner::planCorrection        , &p );

    ros::waitForShutdown();

    return 0;
}
