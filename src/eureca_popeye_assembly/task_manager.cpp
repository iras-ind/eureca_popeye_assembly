#include <eureca_popeye_assembly/task_manager.h>

#include <eigen_conversions/eigen_msg.h>
#include <eigen_conversions/eigen_kdl.h>

#include <moveit_planning_helper/manage_planning_scene.h>
#include <tf/transform_datatypes.h>

#define GET_AND_GO_TO_ERROR( param_name, param )\
if (!nh_.getParam(param_name,param))\
{\
  ROS_ERROR("Parameter '[%s]/[%s]' is not in the ros parame server. Go To Error State.", nh_.getNamespace().c_str(), std::string( param_name ).c_str() );\
  go_to_error_=true;\
  return;\
}\


namespace eureca_popeye_assembly
{
namespace task
{


void StateMachineStructure::entryStateIdle( )
{
}
void StateMachineStructure::entryStatePlan( )
{

  if( !StateMachineStructure::planPrepare( ) )
  {
    is_planning_failed_ = true;
    return;
  }
  
  const std::vector<motion::JointTrajectory::Step> steps= { motion::JointTrajectory::GRASP
                                                          , motion::JointTrajectory::GRASP_FINALIZATION
                                                          , motion::JointTrajectory::PLACE
                                                          , motion::JointTrajectory::PLACE_FINALIZATION
                                                          , motion::JointTrajectory::APPROACH
                                                          , motion::JointTrajectory::CARRY
                                                          , motion::JointTrajectory::GO_TO_REST };

  while(1)
  {
    for( size_t i=0; i<steps.size(); i++)
    {
      const motion::JointTrajectory::Step& step = steps[i];

//      if( !addCarriedCollisionObject( step ) )
//      {
//        ROS_ERROR("ERROR in adding the carriued object to the scene");
//        return;
//      }

      if( step == motion::JointTrajectory::APPROACH )
      {
        target_jconf_[step] = start_jconf_ [motion::JointTrajectory::next( step )];
      }
      else if( step == motion::JointTrajectory::GO_TO_REST )
      {
        start_jconf_ [step] = target_jconf_[motion::JointTrajectory::prev( step )];
      }
      else
      {
        start_jconf_ [step] = target_jconf_[motion::JointTrajectory::prev( step )];
        target_jconf_[step] = start_jconf_ [motion::JointTrajectory::next( step )];
      }

      trajectory_msgs::JointTrajectory res;
      switch(step)
      {
      case motion::JointTrajectory::GRASP:
      case motion::JointTrajectory::GRASP_FINALIZATION:
      case motion::JointTrajectory::PLACE:
      case motion::JointTrajectory::PLACE_FINALIZATION:
      {
        geometry_msgs::Pose start_pose_msg;
        geometry_msgs::Pose target_pose_msg;
        Eigen::Affine3d start_pose;
        Eigen::Affine3d target_pose;
        tf::poseTFToMsg(start_conf_[ step ], start_pose_msg);
        tf::poseMsgToEigen(start_pose_msg, start_pose);

        tf::poseTFToMsg(target_conf_[ step ], target_pose_msg);
        tf::poseMsgToEigen(target_pose_msg, target_pose);

        if (!cart_trj_helper_->computeCartesianTrajectory ( start_pose
                                                          , target_pose
                                                          , execution_time_     [ step ]
                                                          , discretization_pts_ [ step ]
                                                          , res
                                                          , &start_jconf_[ step ] ) )
        {
          ROS_WARN("None trajectory has been found. New trial with starting condiion relaxed");


          if (!cart_trj_helper_->computeCartesianTrajectory ( start_pose
                                                            , target_pose
                                                            , execution_time_ [ step ]
                                                            , discretization_pts_ [ step ]
                                                            , res ) )
          {
            ROS_ERROR("Descartes planning failed also with starting configuration relaxed. NO Cart trajectory can be found.");
            is_planning_failed_=true;;
            return;
          }
          else
          {
              start_jconf_ [step] = res.points.front().positions;
              target_jconf_[step] = res.points.back().positions;
          }
        }
        break;

      }
      break;
      case motion::JointTrajectory::APPROACH:
      case motion::JointTrajectory::CARRY:
      case motion::JointTrajectory::GO_TO_REST:
      {
          moveit::planning_interface::MoveGroupInterface::Plan  plan;
          moveit::core::RobotStatePtr                           start_state = move_group_->getCurrentState();
          start_state->setJointGroupPositions(group_name_, start_jconf_[step]);
          move_group_->setStartState( *start_state );
          move_group_->setJointValueTarget(target_jconf_[step]);

          bool success = (move_group_->plan( plan ) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
          if (!success)
          {
            is_planning_failed_=true;;
            return;
          }

          // RESAMPLING
          trajectory_processing::IterativeSplineParameterization isp;
          for (trajectory_msgs::JointTrajectoryPoint& pnt: plan.trajectory_.joint_trajectory.points)
            pnt.time_from_start=ros::Duration(0);

          robot_trajectory::RobotTrajectory trj(move_group_->getRobotModel(),group_name_);
          trj.setRobotTrajectoryMsg(*start_state,plan.trajectory_);
          isp.computeTimeStamps(trj);
          trj.getRobotTrajectoryMsg(plan.trajectory_);

          res = plan.trajectory_.joint_trajectory;
        }
        break;
      }
      control_msgs::FollowJointTrajectoryGoal tg;
      tg.trajectory = res;
      motion_goals_[ step ].reset(
            new motion::JointTrajectory  ( tg
                                         , ros::Duration( execution_time_ [ step ])
                                         , controller_configuration_[step] )
            );

    }
  }


  is_planning_failed_=false;

  return;
}

void StateMachineStructure::entryStateReplan ( )
{
  entryStatePlan( );
}
void StateMachineStructure::entryStateWait  ( ){ }
void StateMachineStructure::entryStateError ( ){ }

void StateMachineStructure::exitStateInit   ( ){ }
void StateMachineStructure::exitStateIdle   ( ){ }
void StateMachineStructure::exitStatePlan   ( ){ }
void StateMachineStructure::exitStateReplan ( ){ }
void StateMachineStructure::exitStateWait   ( ){ }
void StateMachineStructure::exitStateError  ( ){ }

void StateMachineStructure::entryStateInit()
{
  go_to_error_=false;
  is_planning_failed_=false;
  
  // TODO INIT
  tool_frame_name_ = "ur10_tool0";
  base_frame_name_ = "/world";

  bool use_ikfast=true;
  bool consider_starting_point=true;
  
  GET_AND_GO_TO_ERROR( "group_name"          , group_name_             )
  GET_AND_GO_TO_ERROR( "robot_description"   , robot_description_      )
  
  GET_AND_GO_TO_ERROR( "use_ikfast" , use_ikfast              )
  if( use_ikfast )
  {
    GET_AND_GO_TO_ERROR( group_name_ + "/ikfast_base_frame" , base_frame_name_        )
    GET_AND_GO_TO_ERROR( group_name_ + "/ikfast_tool_frame" , tool_frame_name_        )
  }
  
  GET_AND_GO_TO_ERROR( "consider_starting_point"          , consider_starting_point )
  
  robot_model_loader_.reset( new robot_model_loader::RobotModelLoader ( robot_description_  ) );
  robot_model_        = robot_model_loader_->getModel();
  base_frame_name_    = robot_model_->getJointModelGroup(group_name_)->getSolverInstance()->getBaseFrame();
  tool_frame_name_    = robot_model_->getJointModelGroup(group_name_)->getSolverInstance()->getTipFrame();

  moveit::planning_interface::MoveGroupInterface::Options opts(group_name_, robot_description_);
  move_group_.reset(new moveit::planning_interface::MoveGroupInterface(opts));
  move_group_->startStateMonitor();

  cart_trj_helper_.reset( new descartes::CartesianTrajectoryHelper ( robot_description_
                                                                    , group_name_
                                                                    , base_frame_name_
                                                                    , tool_frame_name_
                                                                    , use_ikfast
                                                                    , consider_starting_point));
  
  visual_tools_.reset( new moveit_visual_tools::MoveItVisualTools(base_frame_name_, "/moveit_visual_markers", robot_model_ ));
  attached_ = false;

  robot_jnames_ = robot_model_->getJointModelGroup(group_name_)->getVariableNames();
  kinematic_state_.reset(new robot_state::RobotState(robot_model_) );
  kinematic_state_->copyJointGroupPositions(robot_model_->getJointModelGroup(group_name_), robot_start_jconf_);
  for (std::size_t i = 0; i < robot_jnames_.size(); ++i)
  {
    ROS_INFO("Start jconf%s: %f", robot_jnames_[i].c_str(), robot_start_jconf_[i]);
  }
}

void StateMachineStructure::actionStorePlanPlanRequest( pickPlanRequest const & e )
{
  pick_place_goal_ = e.pick_place_goal_;
}
void StateMachineStructure::actionSendPlan ( none const & e )
{
  if (!motion_fsm_)
  {
    ROS_ERROR("motion fsm is not initialized");
    is_planning_failed_=true;
  }
  motion_fsm_->process_event( eureca_popeye_assembly::motion::pickRequest( motion_goals_ ) );
  ROS_INFO("Send plan");
}
bool StateMachineStructure::guardIsTimeExpired  ( none const & e )
{
  ROS_INFO("ehi sono passato di qui");
  if (((start_time_ - ros::Time::now()).toSec()<0))
    ROS_ERROR("ma troppo tardi (%f secondi fa) ", (ros::Time::now()-start_time_).toSec());
  else
    ROS_INFO("posso aspettare %f secondi", (start_time_-ros::Time::now()).toSec());
  return ((start_time_-ros::Time::now()).toSec()<0);
}
bool StateMachineStructure::guardIsInError      ( none const & e )
{
  return go_to_error_;
}
bool StateMachineStructure::guardIsPlanFailed   ( none const & e )
{
  return is_planning_failed_;
}
bool StateMachineStructure::guardIsPlanSuccess  ( none const & e )
{
  return !is_planning_failed_;
}
bool StateMachineStructure::guardIsObjectCorrect( pickExecuteRequest const & e )
{
  return true;
}

bool StateMachineStructure::planPrepare( )
{
  ROS_INFO("***** PLAN A PICKING TRAJECTORY ********");
  bool ret = true;
  const std::vector<motion::JointTrajectory::Step> steps= { motion::JointTrajectory::APPROACH
                                                          , motion::JointTrajectory::GRASP
                                                          , motion::JointTrajectory::GRASP_FINALIZATION
                                                          , motion::JointTrajectory::CARRY
                                                          , motion::JointTrajectory::PLACE
                                                          , motion::JointTrajectory::PLACE_FINALIZATION
                                                          , motion::JointTrajectory::GO_TO_REST };

  kinematics::KinematicsBasePtr solver = robot_model_->getJointModelGroup ( group_name_ )->getSolverInstance();
  for(  size_t i=0; i<steps.size(); i++ )
  {
    const motion::JointTrajectory::Step& step = steps[i];
    bool          carried_object_attached = true;   
    eureca_popeye_assembly::SimplePlan plan_data;
    
    ROS_INFO_STREAM(">> Step: " << motion::JointTrajectory::to_string( step ) );
    switch(step)
    {
    case motion::JointTrajectory::APPROACH:
      plan_data = pick_place_goal_.approach;
      carried_object_attached = false;
      break;
    case motion::JointTrajectory::GRASP:
      plan_data = pick_place_goal_.grasp;
      carried_object_attached = false;
      break;
    case motion::JointTrajectory::GRASP_FINALIZATION:
      plan_data = pick_place_goal_.grasp_finalization;
      carried_object_attached = true;
      break;
    case motion::JointTrajectory::CARRY:
      plan_data = pick_place_goal_.carry;
      carried_object_attached = true;
      break;
    case motion::JointTrajectory::PLACE:
      plan_data = pick_place_goal_.place;
      carried_object_attached = true;
      break;
    case motion::JointTrajectory::PLACE_FINALIZATION:
      plan_data = pick_place_goal_.place_finalization;
      carried_object_attached = true;
      break;
    case motion::JointTrajectory::GO_TO_REST:
      plan_data = pick_place_goal_.go_to_rest;
      carried_object_attached = false;
      break;
    }

    carried_object_attached_ [step] = carried_object_attached;
    tf::transformMsgToTF(plan_data.target_frame, target_conf_[step]);

    execution_time_          [step] = plan_data.time;
    discretization_pts_      [step] = plan_data.discretization_pts;
    controller_configuration_[step] = plan_data.controller_configuration;

    if( step == motion::JointTrajectory::APPROACH )
    {
      start_conf_  [step]; //TODO;
      start_jconf_ [step] = robot_start_jconf_;
      target_jconf_[step].resize( solver->getJointNames().size(), 0.0 );
    }
    else
    {
        start_conf_   [step] = target_conf_   [ motion::JointTrajectory::prev( step ) ];
        start_jconf_  [step] = target_jconf_  [ motion::JointTrajectory::prev( step ) ];
        target_jconf_ [step].resize( solver->getJointNames().size(), 0.0 );
    }

    moveit_msgs::MoveItErrorCodes error_code;
    geometry_msgs::Pose pose;
    tf::poseTFToMsg(target_conf_[step], pose);
    ROS_INFO_STREAM("Target Configuration:\n" << to_string( pose ) );
    visual_tools_->publishEEMarkers( pose, robot_model_->getJointModelGroup("clamper") );
    if ( !solver->getPositionIK ( pose, std::vector<double>(robot_model_->getJointModelGroup ( group_name_ )->getActiveJointModelNames().size(),0), target_jconf_[step], error_code ) )
    {
      ROS_ERROR("ERROR in adding the carried object to the scene (err code: %d)" , int(error_code.val) );
      ROS_ERROR("MoveitError: \n>> %s <<\n",  moveit_planning_helper::getMoveitErrorCodesIds( ).at( int( error_code.val ) ).c_str() );
      ROS_ERROR("Seed size: %zu", robot_model_->getJointModelGroup ( group_name_ )->getJointModelNames().size() );
      ROS_ERROR("Seed size: %zu", target_jconf_[step].size() );
     
      ROS_INFO_STREAM("<< Step: " << motion::JointTrajectory::to_string( step ) );
      return false;
    }
    // removeCarriedCollisionObject( );
    ROS_INFO_STREAM("<< Step: " << motion::JointTrajectory::to_string( step ) );
  }
  return ret;
}



}
}
