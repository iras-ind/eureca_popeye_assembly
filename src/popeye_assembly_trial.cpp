#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/conversions.h>
#include <moveit_planning_helper/manage_trajectories.h>
#include <moveit_msgs/ApplyPlanningScene.h>
#include <moveit_msgs/GetPlanningScene.h>
#include <eigen_conversions/eigen_msg.h>
#include <eigen3/Eigen/Core>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <math.h>
#include <shape_msgs/Mesh.h>
#include "geometric_shapes/shape_operations.h"

tf::StampedTransform getTF(const std::string from, const std::string to)
{
  tf::StampedTransform ret;
  tf::TransformListener listener;
  listener.waitForTransform ( from, to, ros::Time ( 0 ), ros::Duration ( 10.0 ) );
  listener.lookupTransform (  from, to, ros::Time ( 0 ), ret );
  return ret;
}

moveit::planning_interface::MoveGroupInterface::Plan plan(moveit::planning_interface::MoveGroupInterface& move_group ,moveit_msgs::RobotState& rs, geometry_msgs::Pose& target)
{
  moveit::planning_interface::MoveGroupInterface::Plan ret;
  move_group.setStartState(rs);
  move_group.setPoseTarget(target);
  move_group.plan(ret);
  rs.joint_state.position = ret.trajectory_.joint_trajectory.points.back().positions;
  return ret;
}
 void getPlanningScene ( ros::NodeHandle& nh
                            , const moveit::core::RobotModelConstPtr robot_model
                            , const std::string& ns
                            , planning_scene::PlanningScene& ret )
    {
        ros::ServiceClient planning_scene_service;
        planning_scene_service = nh.serviceClient<moveit_msgs::GetPlanningScene> ( "get_planning_scene" );
        if ( !planning_scene_service.waitForExistence ( ros::Duration ( 5.0 ) ) )
        {
            ROS_ERROR ( "getPlanningScene Failed: service 'get_planning_scene ' does not exist" );
        }

        moveit_msgs::PlanningScene planning_scene_msgs;

        {
            /// ROBOT_STATE
            moveit_msgs::GetPlanningScene::Request request;
            moveit_msgs::GetPlanningScene::Response response;

            request.components.components = request.components.ROBOT_STATE;
            if ( !planning_scene_service.call ( request, response ) )
            {
                ROS_WARN ( "Could not call planning scene service to get object names" );
            }


            planning_scene_msgs.name = response.scene.name;
            planning_scene_msgs.robot_state = response.scene.robot_state;
        }
        {
            // WORLD_OBJECT_GEOMETRY && WORLD_OBJECT_NAMES
            moveit_msgs::GetPlanningScene::Request request;
            moveit_msgs::GetPlanningScene::Response response;

            request.components.components = request.components.WORLD_OBJECT_GEOMETRY;
            if ( !planning_scene_service.call ( request, response ) )
            {
                ROS_WARN ( "Could not call planning scene service to get object names" );
            }
            planning_scene_msgs.world.collision_objects = response.scene.world.collision_objects;
        }
        {
            // OCTOMAP
            moveit_msgs::GetPlanningScene::Request request;
            moveit_msgs::GetPlanningScene::Response response;

            request.components.components = request.components.OCTOMAP;
            if ( !planning_scene_service.call ( request, response ) )
            {
                ROS_WARN ( "Could not call planning scene service to get object names" );
            }
            planning_scene_msgs.world.octomap = response.scene.world.octomap;
        }
        {
            // TRANSFORMS
            moveit_msgs::GetPlanningScene::Request request;
            moveit_msgs::GetPlanningScene::Response response;

            request.components.components = request.components.TRANSFORMS;
            if ( !planning_scene_service.call ( request, response ) )
            {
                ROS_WARN ( "Could not call planning scene service to get object names" );
            }
            planning_scene_msgs.fixed_frame_transforms = response.scene.fixed_frame_transforms;
        }
        {
            // ALLOWED_COLLISION_MATRIX
            moveit_msgs::GetPlanningScene::Request request;
            moveit_msgs::GetPlanningScene::Response response;

            request.components.components = request.components.ALLOWED_COLLISION_MATRIX;
            if ( !planning_scene_service.call ( request, response ) )
            {
                ROS_WARN ( "Could not call planning scene service to get object names" );
            }
            planning_scene_msgs.allowed_collision_matrix = response.scene.allowed_collision_matrix;
        }
        {
            // LINK_PADDING_AND_SCALING
            moveit_msgs::GetPlanningScene::Request request;
            moveit_msgs::GetPlanningScene::Response response;

            request.components.components = request.components.LINK_PADDING_AND_SCALING;
            if ( !planning_scene_service.call ( request, response ) )
            {
                ROS_WARN ( "Could not call planning scene service to get object names" );
            }
            planning_scene_msgs.link_padding = response.scene.link_padding;
            planning_scene_msgs.link_scale   = response.scene.link_scale;
        }
        {
            // OBJECT_COLORS
            moveit_msgs::GetPlanningScene::Request request;
            moveit_msgs::GetPlanningScene::Response response;

            request.components.components = request.components.LINK_PADDING_AND_SCALING;
            if ( !planning_scene_service.call ( request, response ) )
            {
                ROS_WARN ( "Could not call planning scene service to get object names" );
            }
            planning_scene_msgs.object_colors = response.scene.object_colors;
        }

        ret.setPlanningSceneMsg ( planning_scene_msgs );

        return;
    }

int main(int argc, char **argv)
{
  ros::init(argc, argv, "popeye_assembly_trial");
  ros::NodeHandle nh;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  static const std::string PLANNING_GROUP = "popeye_arm";
  moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
  std::vector<moveit::planning_interface::MoveGroupInterface::Plan> my_plan;
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  
  const robot_state::JointModelGroup* joint_model_group = move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

  ROS_INFO_NAMED("tutorial", "End effector link: %s", move_group.getEndEffectorLink().c_str());
  std::vector<double> home = { 0., 0.38107980886963844, 2.124840831197838, -0.1882376098185855};

  // create the tf    // TODO::load from params
  tf::Pose hatbox_tf;
  {
    hatbox_tf.setOrigin( tf::Vector3(-0.05,0, 0.77 )) ;
    tf::Quaternion q;
    q.setRPY(-45.0*M_PI/180., 0, -M_PI/2.0);
    std::cout<<"q: "<< q.getX()<<", "<< q.getY()<<", "<< q.getZ()<<", "<<  q.getW()<<std::endl;
    hatbox_tf.setRotation(q);
    tf::StampedTransform world_to_cart = getTF("/world", "hatbox_cart");
    hatbox_tf = world_to_cart * hatbox_tf;
  }

  tf::Pose grasping_approach_tf;
  {
    grasping_approach_tf.setOrigin( tf::Vector3(0., 0.1, 0.)) ;
    tf::Quaternion q;
    q.setRPY(10.0 * M_PI /180.0, 0., 0.);
    std::cout<<"q: "<< q.getX()<<", "<< q.getY()<<", "<< q.getZ()<<", "<<  q.getW()<<std::endl;
    grasping_approach_tf.setRotation(q);
    grasping_approach_tf = hatbox_tf * grasping_approach_tf;
  }
  
  tf::Pose hatbox_attach_tf;
  {
    hatbox_attach_tf.setOrigin( tf::Vector3(0, -0.17, 0.52)) ;
    tf::Quaternion q;
    q.setRPY(10.0 * M_PI /180.0, 0., 0.);
    hatbox_attach_tf.setRotation(q);
  }
  
  tf::Pose hatbox_attach_f_tf;
  
  {
    hatbox_attach_f_tf.setOrigin( tf::Vector3(4.35, -0.87, 1.95)) ;
    tf::Quaternion q;
    q.setRPY(0., 0., M_PI);
    hatbox_attach_f_tf.setRotation(q);
    tf::StampedTransform world_to_fus = getTF("/world", "fuselage");
    hatbox_attach_f_tf = world_to_fus * hatbox_attach_f_tf;
  }
  
  tf::Pose hatbox_approach_f_tf;
  {
    hatbox_approach_f_tf.setOrigin( tf::Vector3(0, -0.1, 0.05)) ;
    tf::Quaternion q;
    q.setRPY(-10.0*M_PI/180.0, 0., 0.);
    hatbox_approach_f_tf.setRotation(q);
    hatbox_approach_f_tf = hatbox_attach_f_tf * hatbox_approach_f_tf;
  }
  
  geometry_msgs::Pose hatbox_pose ;
  tf::poseTFToMsg ( hatbox_tf, hatbox_pose );

  //add hatbox
  moveit_msgs::CollisionObject collision_object;
  {
    collision_object.header.frame_id = move_group.getPlanningFrame();
    collision_object.id = "hatbox";
    shapes::Mesh* m = shapes::createMeshFromResource ( "package://eureca_description/meshes/components/collision/hatbox.stl" );

    shape_msgs::Mesh mesh;
    shapes::ShapeMsg mesh_msg;
    shapes::constructMsgFromShape ( m, mesh_msg );
    mesh = boost::get<shape_msgs::Mesh> ( mesh_msg );

    collision_object.meshes.resize ( 1 );
    collision_object.mesh_poses.resize ( 1 );
    collision_object.meshes[0] = mesh;

    collision_object.mesh_poses[0] = hatbox_pose;

    collision_object.meshes.push_back ( mesh );
    collision_object.mesh_poses.push_back ( collision_object.mesh_poses[0] );
    collision_object.operation = collision_object.ADD;

    std::vector<std::string> object_ids_;
    object_ids_.push_back ( collision_object.id );

    std::vector<moveit_msgs::CollisionObject> cov;
    cov.clear();
    cov.push_back ( collision_object );

    robot_model_loader::RobotModelLoader robot_model_loader ( "robot_description" );
    robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
    planning_scene::PlanningScene planning_scene ( kinematic_model );

    ros::Publisher planning_scene_diff_publisher = nh.advertise<moveit_msgs::PlanningScene> ( "planning_scene", 1 );

    while ( planning_scene_diff_publisher.getNumSubscribers() < 1 )
    {
        ros::WallDuration sleep_t ( 0.5 );
        sleep_t.sleep();
    }

    moveit_msgs::PlanningScene planning_scene_msg;

        moveit_msgs::ObjectColor grey;
        std_msgs::ColorRGBA colorGrey;

        colorGrey.r = 42;
        colorGrey.g = 42;
        colorGrey.b = 42;
        colorGrey.a = 1;

        grey.id = collision_object.id;
        grey.color = colorGrey;
        planning_scene_msg.object_colors.push_back ( grey );
        
    for ( moveit_msgs::CollisionObject co: cov )
        planning_scene_msg.world.collision_objects.push_back ( co );

    planning_scene_msg.is_diff = true;
    planning_scene_diff_publisher.publish ( planning_scene_msg );
    ros::Duration ( 1.0 ).sleep();

    ros::ServiceClient planning_scene_diff_client =
    nh.serviceClient<moveit_msgs::ApplyPlanningScene> ( "apply_planning_scene" );
    planning_scene_diff_client.waitForExistence();

    moveit_msgs::ApplyPlanningScene srv;
    srv.request.scene = planning_scene_msg;
    planning_scene_diff_client.call ( srv );

    collision_detection::CollisionRequest collision_request;
    collision_detection::CollisionResult collision_result;

    collision_result.clear();
    
    robot_model::RobotModelPtr robot_model = ( new robot_model_loader::RobotModelLoader ( "robot_description" ) )->getModel();
    getPlanningScene ( nh, robot_model, "", planning_scene );

    planning_scene_msg.robot_state.attached_collision_objects.clear();
    planning_scene_msg.world.collision_objects.clear();

    for ( moveit_msgs::CollisionObject co: cov )
        planning_scene_msg.world.collision_objects.push_back ( co );

    planning_scene_diff_publisher.publish ( planning_scene_msg );
    ros::Duration ( 1.0 ).sleep();
    
    
    ros::Duration(1).sleep();
  }
  // END add hatbox
  
  moveit_msgs::RobotState rs;
  
  robot_model::RobotModelConstPtr robot_model( (new robot_model_loader::RobotModelLoader( "robot_description" ))->getModel() );
  robot_state::RobotState robot_state_start( robot_model ); 

  robot_state_start.setVariablePositions( home );
  robot_state_start.update();
  moveit::core::robotStateToJointStateMsg(robot_state_start,rs.joint_state);
  
  std::cout<<rs<<std::endl;
  
  move_group.setPlanningTime(15.0);
  
  geometry_msgs::Pose target ;
  tf::poseTFToMsg ( grasping_approach_tf, target );
  geometry_msgs::PoseStamped cp = move_group.getCurrentPose();
  target.position.x = cp.pose.position.x;
  std::cout<<"current: "<<move_group.getCurrentPose()<<std::endl;
  std::cout<<"target: " <<target<<std::endl;
  ROS_INFO_STREAM("planning home to approach");
  
  my_plan.push_back(plan(move_group, rs, target));
  ROS_INFO_STREAM("planning approach to grip");
  my_plan.push_back(plan(move_group, rs, hatbox_pose));
  
  std::vector<std::string> clamper_links = {"hatbox_clamper","hatbox_clamper_cad"};
  
  move_group.attachObject ( collision_object.id,clamper_links[0],clamper_links );
  

}
