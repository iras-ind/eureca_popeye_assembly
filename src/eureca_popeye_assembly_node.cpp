#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <cartesian_msgs/FollowCartesianTrajectoryAction.h>
#include <eigen_matrix_utils/eigen_matrix_utils.h>

#include <eigen3/Eigen/Core>
#include <cartesian_trajectory/cartesian_trajectory.h>
#include <eigen_conversions/eigen_msg.h>
#include <descartes_utilities/ros_conversions.h>
#include <moveit_planning_helper/iterative_spline_parameterization.h>
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit/robot_state/conversions.h>
#include <moveit_planning_helper/manage_trajectories.h>
#include <name_sorting/name_sorting.h>

#include <pluginlib/class_loader.h>
#include <ros/ros.h>

// MoveIt!
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit_visual_tools/moveit_visual_tools.h>


int main(int argc, char **argv)
{
  ros::init(argc, argv, "following_cart_action_client");
  ros::NodeHandle nh;
  ros::AsyncSpinner spinner(4);
  spinner.start();


  ROS_INFO("Preparing the environment (getting data from rosparam server");
  std::string robot_description, group_name, base_frame_name, tool_frame_name;
  robot_description ="/robot_description";
  group_name        ="popeye_arm";
  base_frame_name   ="world";
  tool_frame_name   ="clamper";

  moveit::planning_interface::MoveGroupInterface move_group(group_name);
  robot_model::RobotModelPtr  robot_model = robot_model_loader::RobotModelLoader(robot_description).getModel();
  const moveit::core::JointModelGroup* joint_group = robot_model->getJointModelGroup(group_name);


  std::vector< std::vector<double> > jpos{ { 0.0, 10 * M_PI / 180.0, 10 * M_PI / 180.0, 20 * M_PI / 180.0}
                                         //, { 0.0, 20 * M_PI / 180.0, 20 * M_PI / 180.0, -40 * M_PI / 180.0}
                                         , { 0.0, 30 * M_PI / 180.0, 30 * M_PI / 180.0, 60 * M_PI / 180.0} };
  std::vector<double> time{0.0, 5.0, 10.0};

  for( size_t i=0; i<jpos.size(); i++)
  {
    const auto & j = jpos.at( i );
    moveit::core::RobotState kinematic_state(robot_model);
    kinematic_state.setJointGroupPositions(joint_group, j);

    bool valid = kinematic_state.satisfiesBounds();
    ROS_INFO_STREAM("Current state is " << (valid ? "valid" : "not valid"));

    if( !valid)
    {
        ROS_INFO_STREAM("=============== Test 2: enforce bounds");
        kinematic_state.enforceBounds();
    }

    Eigen::Affine3d T_base_start = kinematic_state.getGlobalLinkTransform("clamper");

    geometry_msgs::Pose pose;
    tf::poseEigenToMsg(T_base_start, pose);

     move_group.setPoseTarget(pose);

     moveit::planning_interface::MoveGroupInterface::Plan my_plan;

     bool success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);

     ROS_INFO_NAMED("tutorial", "Visualizing plan 1 (pose goal) %s", success ? "" : "FAILED");
     move_group.execute(my_plan);
  }

  return 0;
}

/*
 *
 *
  Eigen::MatrixXd pose_matrix;
  if (!eigen_utils::getParam(nh,trj_name+"/cart_positions",pose_matrix))
  {
    ROS_ERROR_STREAM(trj_name+"/cart_positions not exist");
    return 0;
  }
  if (pose_matrix.cols()!=7)
  {
    ROS_ERROR("pose matrix dimensions are wrong (number of columns is %zu instead of 7",pose_matrix.cols());
    return 0;
  }
  goal.trajectory.points.resize(pose_matrix.rows());

  std::vector<double> time;
  if (!nh.getParam(trj_name+"/time_from_start",time))
  {
    ROS_ERROR_STREAM(trj_name+"/time_from_start not exist");
    return 0;
  }
  if (time.size()!=pose_matrix.rows())
  {
    ROS_ERROR_STREAM(trj_name+"/time_from_start and cart_trj/cart_positions have different lengths");
    return 0;
  }

  for (unsigned int idx=0;idx<pose_matrix.rows();idx++)
  {
    geometry_msgs::Pose& pose=goal.trajectory.points.at(idx).pose;
    pose.position.x=pose_matrix(idx,0);
    pose.position.y=pose_matrix(idx,1);
    pose.position.z=pose_matrix(idx,2);
    if (std::abs(pose_matrix.block(idx,3,1,4).norm()-1)>0.001)
    {
      ROS_ERROR("%u-th pose has a non unitary quaternion",idx);
      ROS_ERROR_STREAM("quaternion: " << pose_matrix.block(idx,3,1,4).transpose());
      return 0;
    }
    pose.orientation.x=pose_matrix(idx,3);
    pose.orientation.y=pose_matrix(idx,4);
    pose.orientation.z=pose_matrix(idx,5);
    pose.orientation.w=pose_matrix(idx,6);

    goal.trajectory.points.at(idx).time_from_start=ros::Duration(time.at(idx));
  }
*/
