#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <boost/thread.hpp>
#include <eureca_assembly_msgs/Grasp.h>
#include <mutex>
#include <math.h>

tf::TransformBroadcaster br;
tf::Transform actual,not_grasped,grasped;
std::string source;
std::mutex mtx;

bool grasp_set(eureca_assembly_msgs::Grasp::Request  &req,
         eureca_assembly_msgs::Grasp::Response &res)
{
  if(req.grasp)
  {
    mtx.lock();
    source = "hatbox_clamper";
    actual = grasped;
    mtx.unlock();
    ROS_INFO("Hatbox grasped");
  }
  return true;
}

int main(int argc, char **argv)
{

  not_grasped.setOrigin( tf::Vector3(-0.05,0, 0.77 ));
  tf::Quaternion q;
  q.setRPY(-45.0*M_PI/180., 0, -M_PI/2.0);
  not_grasped.setRotation(q);

  grasped.setOrigin( tf::Vector3(0.0,0.0,0.0 ));
  tf::Quaternion q2;
  q2.setRPY(0.0,0.0, 0.0);
  grasped.setRotation(q2);

  ros::init(argc, argv, "grasp_pub");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("grasping", grasp_set);
  ros::spin();

  actual = not_grasped;
  source = "hatbox_cart";

  ros::waitForShutdown();
}

void pub_tf()
{
   ros::Rate RT(20);
   while(ros::ok())
   {
     mtx.lock();
     br.sendTransform(tf::StampedTransform(actual, ros::Time::now(), source, "hatbox"));
     mtx.unlock();
     RT.sleep();
   }
}
