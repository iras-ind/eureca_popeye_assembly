#ifndef pickplace_task__201809131033
#define pickplace_task__201809131033

#include <moveit_visual_tools/moveit_visual_tools.h>

#include <eureca_popeye_assembly/common.h>
#include <actionlib/client/simple_action_client.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit/collision_detection/collision_matrix.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

#include <eureca_popeye_assembly/PickExecuteAction.h>
#include <eureca_popeye_assembly/PickPlanAction.h>
#include <eureca_popeye_assembly/motion_manager.h>


#include <cartesian_trajectory/cartesian_trajectory.h>
#include <moveit_planning_helper/iterative_spline_parameterization.h>
#include <moveit_planning_helper/manage_trajectories.h>


namespace msm = boost::msm;
namespace mpl = boost::mpl;
using namespace msm::front;

using namespace msm::front::euml;

namespace eureca_popeye_assembly
{
namespace task
{
  
  
  // events
  struct pickPlanRequest
  {
    eureca_popeye_assembly::PickPlanGoal pick_place_goal_;
    pickPlanRequest(const eureca_popeye_assembly::PickPlanGoal& goal)
    {
      pick_place_goal_=goal;
    }
  };
  
  struct pickExecuteRequest
  {
    eureca_popeye_assembly::PickExecuteGoal exec_goal_;
    pickExecuteRequest(const eureca_popeye_assembly::PickExecuteGoal& goal)
    {
      exec_goal_=goal;
    }
  };
  
  struct pickCancelRequest
  {
    pickCancelRequest(const eureca_popeye_assembly::PickExecuteGoal& goal)
    {
    }
  };

  struct update {};

  // front-end: define the FSM structure 
  struct StateMachineStructure : public msm::front::state_machine_def<StateMachineStructure>
  {
    StateMachineStructure( ros::NodeHandle& nh )  : nh_(nh) {  }
    ros::NodeHandle nh_;
    moveit_visual_tools::MoveItVisualToolsPtr visual_tools_;

    eureca_popeye_assembly::PickPlanGoal pick_place_goal_;

    std::map<motion::JointTrajectory::Step, std::shared_ptr<motion::JointTrajectory> >    motion_goals_;
    std::map<motion::JointTrajectory::Step, tf::Pose >                                    start_conf_;
    std::map<motion::JointTrajectory::Step, tf::Pose >                                    target_conf_;
    std::map<motion::JointTrajectory::Step, std::vector<double> >                         start_jconf_;
    std::map<motion::JointTrajectory::Step, std::vector<double> >                         target_jconf_;
    std::map<motion::JointTrajectory::Step, double >                                      carried_object_attached_;
    std::map<motion::JointTrajectory::Step, std::string         >                         carried_object_pose_;
    std::map<motion::JointTrajectory::Step, std::string         >                         controller_configuration_;
    std::map<motion::JointTrajectory::Step, collision_detection::AllowedCollisionMatrix > acm_;
    std::map<motion::JointTrajectory::Step, double >                                      execution_time_;
    std::map<motion::JointTrajectory::Step, double >                                      discretization_pts_;


    std::string path_to_mesh_;
    ros::Time  start_time_;
    ros::Time end_of_last_movement_;

    bool is_planning_failed_;
    std::string                                           robot_description_;
    moveit::planning_interface::MoveGroupInterfacePtr     move_group_;
    std::shared_ptr<robot_model_loader::RobotModelLoader> robot_model_loader_;
    robot_model::RobotModelPtr                            robot_model_;
    robot_state::RobotStatePtr                            kinematic_state_;
    std::string                                           robot_tip_frame_;
    moveit::planning_interface::PlanningSceneInterface    planning_scene_interface_;
    std::shared_ptr<descartes::CartesianTrajectoryHelper> cart_trj_helper_;

    tf::TransformListener listener_;
    tf::TransformBroadcaster br_;
    std::string group_name_;
    std::string tool_frame_name_;
    std::string base_frame_name_;
    std::vector<std::string> robot_jnames_;
    std::vector<double> robot_start_jconf_;
    bool attached_;
    bool go_to_error_;
    std::shared_ptr<eureca_popeye_assembly::motion::StateMachine > motion_fsm_;
    void setMotionManager(std::shared_ptr<eureca_popeye_assembly::motion::StateMachine>& motion_fsm)
    {
      motion_fsm_=motion_fsm;
    }

    bool planPrepare( );
//    bool addCarriedCollisionObject(const motion::JointTrajectory::Step& step);
//    bool removeCarriedCollisionObject( );

    PREPARE_LOGGER("TPM")

    TYPEDEF_SIMPLE_STATE(Init  , "INFO" )
    TYPEDEF_SIMPLE_STATE(Idle  , "INFO" )
    TYPEDEF_SIMPLE_STATE(Plan  , "INFO" )
    TYPEDEF_SIMPLE_STATE(Replan, "INFO" )
    TYPEDEF_SIMPLE_STATE(Wait  , "INFO" )
    TYPEDEF_SIMPLE_STATE(Error , "ERROR" )
    
    // the initial state of the player SM. Must be defined
    typedef Init initial_state;
    
    TYPEDEF_SIMPLE_ACTION( StorePlanPlanRequest, pickPlanRequest   , "INFO" )
    TYPEDEF_SIMPLE_ACTION( SendPlan            , none              , "INFO" )

    TYPEDEF_SIMPLE_GUARD ( IsTimeExpired       , none              , "ERROR" )
    TYPEDEF_SIMPLE_GUARD ( IsInError           , none              , "ERROR" )
    TYPEDEF_SIMPLE_GUARD ( IsPlanFailed        , none              , "ERROR" )
    TYPEDEF_SIMPLE_GUARD ( IsPlanSuccess       , none              , "INFO"  )
    TYPEDEF_SIMPLE_GUARD ( IsObjectCorrect     , pickExecuteRequest, "INFO"  )


    typedef StateMachineStructure task_p; // makes transition table cleaner
    std::vector<std::string> state_names =  { StateID_Init
                                            , StateID_Idle
                                            , StateID_Plan
                                            , StateID_Replan
                                            , StateID_Wait
                                            , StateID_Error   };
    
    // Transition table for player
    struct transition_table : mpl::vector<
    //    Start    Event              Next      Action                Guard
    //  +---------+-----------------+---------+----------------------+----------------------+
    Row < Init,    none,               Idle,     none,                 none>,
    Row < Idle,    pickPlanRequest,    Plan,     StorePlanPlanRequest, none>,
    Row < Idle,    none,               Error,    none,                 IsInError>,
    
    Row < Plan,    none,               Replan,   none,                 IsPlanFailed>,
    Row < Plan,    none,               Wait,     none,                 IsPlanSuccess>,
    Row < Plan,    none,               Error,    none,                 IsInError>,
    
    Row < Replan,  none,               Idle,     none,                 IsPlanFailed>,
    Row < Replan,  none,               Wait,     none,                 IsPlanSuccess>,
    Row < Replan,  none,               Error,    none,                 IsInError>,
    
    Row < Wait,    pickExecuteRequest, Idle,     SendPlan,             IsObjectCorrect>,
    Row < Wait,    pickCancelRequest,  Idle,     none,                 none>,
    Row < Wait,    update,             Idle,     none,                 IsTimeExpired>,
    Row < Wait,    none,               Idle,     none,                 IsTimeExpired>,
    Row < Wait,    none,               Error,    none,                 IsInError>
    
    //  +---------+-------------+---------+---------------------------+----------------------+
    > {};
    // Replaces the default no-transition response.
    template <class FSM,class Event>
    void no_transition(Event const& e, FSM& f,int state)
    {
      if (f.go_to_error_)
        ROS_ERROR("in error state");
    }
    void move_group();
    
  };
  
  // Pick a back-end
  typedef msm::back::state_machine<StateMachineStructure> StateMachine;
  inline std::string getStateName(StateMachine const& p)
  {
    return p.state_names[p.current_state()[0]];
  }

}
}

#endif
