#ifndef pickplace__201809071025
#define pickplace__201809071025

#include <eureca_popeye_assembly/common.h>

#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/robot_state/conversions.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_listener.h>
#include <diagnostic_updater/diagnostic_updater.h>


namespace msm = boost::msm;
namespace mpl = boost::mpl;
using namespace msm::front;

using namespace msm::front::euml;

namespace eureca_popeye_assembly
{
namespace motion
{

  struct JointTrajectory
  {
    enum Step  { APPROACH, GRASP, GRASP_FINALIZATION, CARRY, PLACE, PLACE_FINALIZATION, GO_TO_REST};
    static const std::vector< Step > sequence()
    {
      static const std::vector<Step>& steps =
          { motion::JointTrajectory::APPROACH
          , motion::JointTrajectory::GRASP
          , motion::JointTrajectory::GRASP_FINALIZATION
          , motion::JointTrajectory::CARRY
          , motion::JointTrajectory::PLACE
          , motion::JointTrajectory::PLACE_FINALIZATION
          , motion::JointTrajectory::GO_TO_REST };
      return steps;
    }
    static const std::vector< std::string > ids()
    {
      static const std::vector< std::string >& steps =
          { "APPROACH"
          , "GRASP"
          , "GRASP_FINALIZATION"
          , "CARRY"
          , "PLACE"
          , "PLACE_FINALIZATION"
          , "GO_TO_REST" };
      return steps;
    }
    static Step prev( Step step )
    {
      auto const & steps =sequence();
      auto const it = std::find( steps.begin(), steps.end(), step );
      return ( it == steps.begin() ) ? step : *(it -1 );
    }
    static Step next( Step step )
    {
      auto const & steps =sequence();
      auto const it = std::find( steps.begin(), steps.end(), step );
      return ( it == steps.end() ) ? step : *(it + 1 );
    }
    static std::string to_string( Step step )
    {
      auto const & steps = sequence();
      auto const & id    = ids();
      auto const it = std::find( steps.begin(), steps.end(), step );
      return id.at(std::distance( steps.begin(), it ) );
    }
    const control_msgs::FollowJointTrajectoryGoal goal_;
    const std::string                             controller_configuration_;
    const ros::Duration                           t_;
    JointTrajectory ( const control_msgs::FollowJointTrajectoryGoal&  goal
                    , const ros::Duration&                            t
                    , const std::string&                              controller_configuration )
          : goal_                     ( goal )
          , t_                        ( t )
          , controller_configuration_ ( controller_configuration )
        { }
    JointTrajectory ( const JointTrajectory&  cpy )
          : goal_                     ( cpy.goal_ )
          , t_                        ( cpy.t_ )
          , controller_configuration_ ( cpy.controller_configuration_ )
        { }
  };
  // events
  struct pickRequest
  {
    const std::map<JointTrajectory::Step,std::shared_ptr<motion::JointTrajectory> > goals_;
    pickRequest ( const std::map<JointTrajectory::Step,std::shared_ptr<JointTrajectory> >& goals ) : goals_ ( goals )       { }
    pickRequest ( const pickRequest& cpy   ) : goals_ ( cpy.goals_ )  { }
  };

  struct update {};
  struct EventNone {};

  /**
   *
   * front-end: define the FSM structure
   *
   *
   */
  struct StateMachineStructure : public msm::front::state_machine_def<StateMachineStructure>
  {
    StateMachineStructure( ros::NodeHandle& nh  ) : nh_( nh ) {}

    bool go_to_error_;
    ros::NodeHandle nh_;

    std::map<motion::JointTrajectory::Step, std::shared_ptr< motion::JointTrajectory> > trajectories_;
    ros::Time start_goal_;

    std::shared_ptr< actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> > trj_action_;
    tf::TransformListener     listener_;
    tf::StampedTransform      tool_transform_;
    std::string               object_frame_;
    std::string               tool_frame_name_;

    PREPARE_LOGGER( "MPM" )

    TYPEDEF_SIMPLE_STATE( Error            , "ERROR" )
    TYPEDEF_SIMPLE_STATE( Init             , "INFO"  )
    TYPEDEF_SIMPLE_STATE( Rest             , "INFO"  )
    TYPEDEF_SIMPLE_STATE( Ready            , "INFO"  )
    TYPEDEF_SIMPLE_STATE( Approach         , "INFO"  )
    TYPEDEF_SIMPLE_STATE( Grasp            , "INFO"  )
    TYPEDEF_SIMPLE_STATE( CloseGripper     , "INFO"  )
    TYPEDEF_SIMPLE_STATE( GraspFinalization, "INFO"  )
    TYPEDEF_SIMPLE_STATE( Carry            , "INFO"  )
    TYPEDEF_SIMPLE_STATE( Place            , "INFO"  )
    TYPEDEF_SIMPLE_STATE( PlaceFinalization, "INFO"  )
    TYPEDEF_SIMPLE_STATE( OpenGripper      , "INFO"  )

    TYPEDEF_SIMPLE_ACTION( CachePlan             , pickRequest, "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartApproach         , none       , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartGrasp            , update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( TriggerCloseGripper   , update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartGraspFinalization, update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartCarry            , none       , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartPlace            , update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( FinalizePlace         , update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( TriggerOpenGripper    , none       , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( GoToRest              , none       , "INFO"  )

    TYPEDEF_SIMPLE_GUARD( IsMovementFinished     , update      , "INFO" )
    TYPEDEF_SIMPLE_GUARD( IsGrasped              , update      , "INFO" )
    TYPEDEF_SIMPLE_GUARD( IsPlaced               , none        , "INFO" )
    TYPEDEF_SIMPLE_GUARD( IsGraspingFinished     , update      , "INFO" )
    TYPEDEF_SIMPLE_GUARD( IsPickingFinished      , update      , "INFO" )
    TYPEDEF_SIMPLE_GUARD( IsInError              , update      , "INFO" )

    // the initial state of the player SM. Must be defined
    typedef Init initial_state;
    typedef StateMachineStructure p; // makes transition table cleaner
    std::vector<std::string>  state_names = { StateID_Error
                                            , StateID_Init
                                            , StateID_Rest
                                            , StateID_Ready
                                            , StateID_Approach
                                            , StateID_Grasp
                                            , StateID_CloseGripper
                                            , StateID_GraspFinalization
                                            , StateID_Carry
                                            , StateID_Place
                                            , StateID_PlaceFinalization
                                            , StateID_OpenGripper };

    // Transition table NOTE: the transition table is checked from the bottom to the top
    struct transition_table : mpl::vector<
    //    Start             Event         Next                  Action                        Guard
    //  +----------        +--------------+------------------+---------------------   +----------------------+
    Row < Init             , none         , Rest             ,  none                  , none                   >,
    Row < Init             , update       , Error            ,  none                  , IsInError              >,
    Row < Rest             , pickRequest  , Ready            ,  CachePlan             , none                   >,
    Row < Rest             , update       , Error            ,  none                  , IsInError              >,

    Row < Ready            , none         , Approach         ,  StartApproach         , none                   >,
    Row < Ready            , update       , Error            ,  none                  , IsInError              >,

    Row < Approach         , update       , Grasp            ,  StartGrasp            , IsMovementFinished     >,
    Row < Approach         , update       , Error            ,  none                  , IsInError              >,

    Row < Grasp            , update       , CloseGripper     ,  TriggerCloseGripper   , none                   >,
    Row < Grasp            , update       , Error            ,  none                  , IsInError              >,

    Row < CloseGripper     , update       , GraspFinalization,  StartGraspFinalization, IsGrasped              >,
    Row < CloseGripper     , update       , Error            ,  none                  , IsInError              >,

    Row < GraspFinalization, none         , Carry            ,  StartCarry            , none                   >,
    Row < GraspFinalization, update       , Error            ,  none                  , IsInError              >,

    Row < Carry            , update       , Place            ,  StartPlace            , IsMovementFinished     >,
    Row < Carry            , update       , Error            ,  none                  , IsInError              >,

    Row < Place            , update       , PlaceFinalization,  FinalizePlace         , IsMovementFinished     >,
    Row < Place            , update       , Error            ,  none                  , IsInError              >,

    Row < PlaceFinalization, none         , OpenGripper      ,  TriggerOpenGripper    , IsPlaced               >,
    Row < PlaceFinalization, update       , Error            ,  none                  , IsInError              >,

    Row < OpenGripper      , none         , Rest             ,  GoToRest              , none                   >,
    Row < OpenGripper      , update       , Error            ,  none                  , IsInError              >
    > {};

    // Replaces the default no-transition response.
    template <class FSM,class Event>
    void no_transition(Event const& e, FSM& f,int state)
    {
      if (f.go_to_error_)
        ROS_ERROR("in error state");
    }
  };

  // Pick a back-end
  typedef msm::back::state_machine<StateMachineStructure> StateMachine;

  //
  // Testing utilities.
  //
  inline std::string getStateName(StateMachine const& p)
  {
    return p.state_names[p.current_state()[0]];
  }

}
}

#endif
