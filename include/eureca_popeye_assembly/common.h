#ifndef COMMON_H
#define COMMON_H

#include <mutex>
#include <eigen3/Eigen/Core>
#include <tf_conversions/tf_eigen.h>

#include <eigen_conversions/eigen_msg.h>
#include <geometry_msgs/Pose.h>
#include <iostream>
#include <sstream>
#include <boost/mpl/vector.hpp>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/state_machine_def.hpp>
#include <boost/msm/front/functor_row.hpp>
#include <boost/msm/front/euml/common.hpp>
#include <boost/msm/front/euml/operator.hpp>
#include <boost/msm/front/euml/state_grammar.hpp>

#include <ros/ros.h>

inline void foo(void){}

#define PREPARE_LOGGER( ID_NAME )\
  constexpr static const char*  id_ = ID_NAME;\
  constexpr static const bool   verbose_ = true;\
  std::mutex                    mtx_;\
  std::vector<std::string>      logs_;\
  std::vector<std::string>      getLogs()\
  {\
    mtx_.lock();\
    std::vector<std::string>  ret = logs_;\
    logs_.clear();\
    mtx_.unlock();\
    return ret;\
  }

#define LOG_MSG(MSG,MSG_SEVERITY)\
  f.mtx_.lock();\
  if      ( f.verbose_ && MSG_SEVERITY == "INFO" )  ROS_INFO( "[%s] %s", f.id_, MSG.c_str() );\
  else if ( f.verbose_ && MSG_SEVERITY == "WARN" )  ROS_WARN( "[%s] %s", f.id_, MSG.c_str() );\
  else if ( f.verbose_ && MSG_SEVERITY == "ERROR" ) ROS_ERROR("[%s] %s", f.id_, MSG.c_str() );\
  else if ( f.verbose_ )                            ROS_FATAL("[%s] %s", f.id_, MSG.c_str() );\
  f.logs_.push_back(MSG);\
  f.mtx_.unlock();\


#define TYPEDEF_SIMPLE_STATE( NAME, MSG_SEVERITY )\
  const std::string StateID_##NAME = #NAME;\
\
\
  struct NAME##_Tag\
  {\
  };\
\
\
  struct NAME##_Entry\
  {\
    template <class Event,class FSM,class STATE>\
    void operator()(Event const& e,FSM& f,STATE& s)\
    {\
      LOG_MSG( std::string( "State '" + std::string( #NAME ) + "', ENTRY - START" ),MSG_SEVERITY);\
      f.entryState##NAME( );\
      LOG_MSG( std::string( "State '" + std::string( #NAME ) + "', ENTRY - DONE" ),MSG_SEVERITY);\
    }\
  };\
\
\
  struct NAME##_Exit\
  {\
      template <class Event,class FSM,class STATE>\
      void operator()(Event const& e,FSM& f,STATE& s)\
      {\
      LOG_MSG( std::string( "State '" + std::string( #NAME ) + "', EXIT - START" ),MSG_SEVERITY);\
      f.exitState##NAME( );\
      LOG_MSG( std::string( "State '" + std::string( #NAME ) + "', EXIT - DONE" ),MSG_SEVERITY);\
      }\
  };\
\
\
  typedef msm::front::euml::func_state<NAME##_Tag,NAME##_Entry,NAME##_Exit> NAME;\
  virtual void entryState##NAME( );\
  virtual void exitState##NAME ( );\



//////////////////////////////////
#define TYPEDEF_SIMPLE_ACTION( NAME, EventType, MSG_SEVERITY  )\
  virtual void action##NAME( EventType const& e);\
  struct NAME\
  {\
    template <class EVT,class FSM,class SourceState,class TargetState>\
    void operator()(EVT const& e,FSM& f,SourceState& s,TargetState& t)\
    {\
      LOG_MSG( std::string( "Action '" + std::string( #NAME ) + "', START" ),MSG_SEVERITY);\
      f.action##NAME(e);\
      LOG_MSG( std::string( "Action '" + std::string( #NAME ) + "', DONE" ),MSG_SEVERITY);\
    }\
  };


#define TYPEDEF_SIMPLE_GUARD( NAME, EventType, MSG_SEVERITY   )\
  virtual bool guard##NAME( EventType const& e );\
  struct NAME\
  {\
    template <class EVT,class FSM,class SourceState,class TargetState>\
    bool operator()(EVT const& e,FSM& f,SourceState& s,TargetState& t)\
    {\
      LOG_MSG( std::string( "Guard '" + std::string( #NAME ) + "', START" ),MSG_SEVERITY);\
      bool ret = f.guard##NAME( e );\
      LOG_MSG( std::string( "Guard '" + std::string( #NAME ) + "', DONE" ),MSG_SEVERITY);\
      return ret;\
    }\
  };






inline std::ostream& operator<<(std::ostream& stream, const Eigen::Affine3d& affine)
{
    stream.precision(6);
    stream.width (7);
    stream << "\e[1mRotation:\e[0m"    << std::endl << std::fixed << affine.rotation() << std::endl;
    stream << "\e[1mTranslation:\e[0m" << std::endl << std::fixed << affine.translation().transpose() << std::endl;

    return stream;
}

inline std::ostream& operator<<(std::ostream& stream, const tf::Transform& transform)
{
  Eigen::Affine3d affine;
  tf::transformTFToEigen( transform, affine );
  stream << affine;
  return stream;
}

inline std::string to_string(const geometry_msgs::Pose& pose)
{
  std::stringstream str;
  Eigen::Affine3d affine;
  tf::poseMsgToEigen( pose, affine );
  str << affine;
  return str.str();
}


inline const std::string RESET        ( ) { return "\033[0m";         };
inline const std::string BLACK        ( ) { return "\033[30m";        };
inline const std::string RED          ( ) { return "\033[31m";        };
inline const std::string GREEN        ( ) { return "\033[32m";        };
inline const std::string YELLOW       ( ) { return "\033[33m";        };
inline const std::string BLUE         ( ) { return "\033[34m";        };
inline const std::string MAGENTA      ( ) { return "\033[35m";        };
inline const std::string CYAN         ( ) { return "\033[36m";        };
inline const std::string WHITE        ( ) { return "\033[37m";        };
inline const std::string BOLDBLACK    ( ) { return "\033[1m\033[30m"; };
inline const std::string BOLDRED      ( ) { return "\033[1m\033[31m"; };
inline const std::string BOLDGREEN    ( ) { return "\033[1m\033[32m"; };
inline const std::string BOLDYELLOW   ( ) { return "\033[1m\033[33m"; };
inline const std::string BOLDBLUE     ( ) { return "\033[1m\033[34m"; };
inline const std::string BOLDMAGENTA  ( ) { return "\033[1m\033[35m"; };
inline const std::string BOLDCYAN     ( ) { return "\033[1m\033[36m"; };
inline const std::string BOLDWHITE    ( ) { return "\033[1m\033[37m"; };



#endif // COMMON_H
